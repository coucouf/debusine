# debusine

debusine is a general purpose software factory tailored to the needs of a
Debian-based distribution. It brings together:
- an artifact storage system
- a task scheduler
- a workflow management system

It aims to be flexible enough to handle:
- package building (and replace buildd+wanna-build)
- package QA (and replace the deceased [mole](https://wiki.debian.org/Mole)
  and [debile](https://github.com/opencollab/debile) services)
- specific validation workflows for package uploads (think checks made by
  security teams before releasing an update)
- many of the time-consuming tasks that are run by Distro Tracker and
  other QA tools

## Why this project?

Have a look at [the dedicated page in the documentation](https://freexian-team.pages.debian.net/debusine/devel/why.html).

## Documentation

The [documentation](https://freexian-team.pages.debian.net/debusine/) always
matches what's in the git repository's master branch.

Otherwise you can generate the documentation yourself by doing `make html`
in the docs subdirectory of the debusine git repository.

## Interacting with the project

### How to contribute

Have a look at the ["Contributing"
section](https://freexian-team.pages.debian.net/debusine/devel/contributing.html) of the
documentation.

### Contact information

You can interact with the developers through the [issue
tracker](https://salsa.debian.org/freexian-team/debusine/-/issues)
or on the `#debusine` IRC channel on the OFTC network (irc.debian.org
server for example).

The lead developer is Raphaël Hertzog (buxy on IRC).

### Reporting bugs and vulnerabilities

We are using [GitLab's bug
tracker](https://salsa.debian.org/freexian-team/debusine/-/issues) to
manage bug reports which are related to the source code itself. You should
file new bugs there.

Security issues should be reported to the bug tracker like other bugs
but they can be marked as confidential if the issue is really sensitive.
If you are unsure, start with a confidential issue, we can always
make it public later.
