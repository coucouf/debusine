#!/bin/sh

set -e

# Set up debusine-worker: copies the example config.ini and prints
# the token (disabled at this moment) in the stdout

debusine_worker_config_directory=/etc/debusine/worker

cp /usr/share/doc/debusine-worker/examples/config.ini "$debusine_worker_config_directory"
chown debusine-worker:debusine-worker "$debusine_worker_config_directory"

systemctl restart debusine-worker

debusine_worker_token_file="$debusine_worker_config_directory/token"

# Wait up to 15 seconds for the token file to appear
count=0
while [ ! -f "$debusine_worker_token_file" ] && [ $count -lt 15 ]
do
  sleep 1
  count=$((count + 1))
done

cat "$debusine_worker_token_file"
