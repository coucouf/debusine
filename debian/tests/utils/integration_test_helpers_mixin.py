# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.
"""Helper mixin for the integration tests."""
import subprocess
import tempfile
from pathlib import Path

from .client import Client


class IntegrationTestHelpersMixin:
    """Utility methods for the integration tests."""

    @staticmethod
    def _create_artifact(
        package_name: str, apt_get_command: str, artifact_type: str
    ) -> int:
        """
        Create an artifact.

        :param package_name: package contained in the artifact.
        :param apt_get_command: e.g. "source", "download".
        :param artifact_type: type of artifact (e.g. debian:source-package,
          debian:binary-packages).
        """
        with tempfile.TemporaryDirectory(
            prefix="debusine-integration-tests-"
        ) as temp_directory:
            temp_directory = Path(temp_directory)

            subprocess.check_call(
                ["apt-get", apt_get_command, "--download-only", package_name],
                cwd=temp_directory,
            )

            files = temp_directory.iterdir()

            artifact_id = Client.execute_command(
                "create-artifact",
                artifact_type,
                "--workspace",
                "System",
                "--upload",
                *files,
            )["artifact_id"]

            return artifact_id

    @classmethod
    def create_artifact_source(cls, package_name: str) -> int:
        """Create a source artifact with hello files."""
        return cls._create_artifact(
            package_name,
            "source",
            "debusine:source-package",
        )

    @classmethod
    def create_artifact_binary(cls, package_name: str) -> int:
        """Create a binary artifact."""
        return cls._create_artifact(
            package_name, "download", "debusine:binary-package"
        )
