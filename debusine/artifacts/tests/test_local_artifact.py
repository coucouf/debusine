# Copyright 2023 The Debusine developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Unit tests for classes in local_artifacts.py."""

import json
from datetime import datetime, timedelta
from pathlib import Path
from unittest import TestCase, mock

import debian.deb822 as deb822

from debusine.artifacts import (
    BinaryPackages,
    BinaryUpload,
    LintianArtifact,
    LocalArtifact,
    PackageBuildLog,
    SourcePackage,
    WorkRequestDebugLogs,
)
from debusine.artifacts.local_artifact import (
    AutopkgtestArtifact,
    deb822dict_to_dict,
)
from debusine.client.models import FileRequest
from debusine.test import TestHelpersMixin


class LocalArtifactTests(TestHelpersMixin, TestCase):
    """Tests for LocalArtifact class."""

    def setUp(self):
        """Set up test."""
        self.category = "category"
        self.data = {"time": "24"}
        self.files = {}

        self.local_artifact = LocalArtifact(
            category=self.category,
            files=self.files,
            data=self.data,
        )

    def test_add_local_file_in_root(self):
        """add_local_file() add the file in .files."""
        file = self.create_temporary_file()
        self.local_artifact.add_local_file(file)

        self.assertEqual(
            self.local_artifact.files,
            {file.name: file},
        )

    def test_add_local_file_in_override_name(self):
        """add_local_file() with override_name."""
        file = self.create_temporary_file()

        name = "new_name.txt"
        self.local_artifact.add_local_file(file, override_name=name)

        self.assertEqual(self.local_artifact.files, {name: file})

    def test_add_local_file_artifact_base_dir_does_not_exist(self):
        """add_local_file() raise ValueError: directory does not exist."""
        file = self.create_temporary_file()
        artifact_base_dir = Path("/does/not/exist")
        with self.assertRaisesRegex(
            ValueError,
            f'"{artifact_base_dir}" does not exist or is not a directory',
        ):
            self.local_artifact.add_local_file(
                file, artifact_base_dir=artifact_base_dir
            )

    def test_add_local_file_relative_to_artifact_base_dir_does_not_exist(self):
        """add_local_file() raise ValueError: filename does not exist."""
        artifact_base_dir = self.create_temporary_directory()
        filename = Path("README.txt")

        filename_absolute = artifact_base_dir / filename

        with self.assertRaisesRegex(
            ValueError, f'"{filename_absolute}" does not exist'
        ):
            self.local_artifact.add_local_file(
                filename, artifact_base_dir=artifact_base_dir
            )

    def test_add_local_file_absolute_does_not_exist(self):
        """add_local_file() raise ValueError: filename does not exist."""
        empty_dir = self.create_temporary_directory()

        filename = empty_dir / "a-file-does-not-exist.txt"

        with self.assertRaisesRegex(ValueError, f'"{filename}" does not exist'):
            self.local_artifact.add_local_file(filename)

    def test_add_local_file_is_not_a_file(self):
        """add_local_file() raise ValueError: expected file is a directory."""
        empty_dir = self.create_temporary_directory()

        with self.assertRaisesRegex(ValueError, f'"{empty_dir}" is not a file'):
            self.local_artifact.add_local_file(empty_dir)

    def test_add_local_file_relative_to_artifact_base_dir(self):
        """add_local_file() add relative file from an absolute directory."""
        artifact_base_dir = self.create_temporary_directory()
        filename = Path("README.txt")

        (file := artifact_base_dir / filename).write_bytes(b"")

        self.local_artifact.add_local_file(
            filename, artifact_base_dir=artifact_base_dir
        )

        self.assertEqual(self.local_artifact.files, {'README.txt': file})

    def test_add_local_file_artifact_raise_value_error_dir_is_relative(self):
        """add_local_file() raise ValueError if base_dir is relative."""
        directory = Path("does-not-exist")
        file = directory / "README.txt"
        with self.assertRaisesRegex(
            ValueError, f'"{directory}" must be absolute'
        ):
            self.local_artifact.add_local_file(
                file, artifact_base_dir=directory
            )

    def test_add_local_file_absolute_path_no_artifact_base_dir(self):
        """add_local_file() adds file. No artifact path or artifact_dir."""
        file = self.create_temporary_file()

        self.local_artifact.add_local_file(file)

        self.assertEqual(
            self.local_artifact.files,
            {file.name: file},
        )

    def test_add_local_file_duplicated_raise_value_error(self):
        """add_local_file() raise ValueError: file is already added."""
        file = self.create_temporary_file()
        self.local_artifact.add_local_file(file)

        with self.assertRaisesRegex(
            ValueError,
            rf"^File with the same path \({file.name}\) "
            rf'is already in the artifact \("{file}"\ and "{file}"\)',
        ):
            self.local_artifact.add_local_file(file)

    def test_serialize_for_create(self):
        """serialize_for_create_artifact return the expected dictionary."""
        workspace = "workspace"
        self.assertEqual(
            self.local_artifact.serialize_for_create_artifact(
                workspace=workspace
            ),
            {
                "category": self.category,
                "data": self.data,
                "expire_at": None,
                "files": self.files,
                "workspace": workspace,
                "work_request": None,
            },
        )

    def test_serialize_for_create_no_workspace(self):
        """serialize_for_create_artifact return dictionary without workspace."""
        self.assertNotIn(
            "workspace",
            self.local_artifact.serialize_for_create_artifact(workspace=None),
        )

    def test_validate_model_on_serialization(self):
        """serialize_for_create_artifact raise ValueError: invalid artifact."""
        file = self.create_temporary_file(suffix=".changes")
        artifact = BinaryUpload(files={file.name: file}, category="Test")

        # Remove the only file. The "artifact" object will not validate
        artifact.files.popitem()

        with self.assertRaisesRegex(ValueError, "^Model validation failed:"):
            artifact.serialize_for_create_artifact(workspace="System")

    def test_serialize_for_create_with_work_request(self):
        """serialize_for_create_artifact include the work_request."""
        workspace = "workspace"
        work_request = 5

        serialized = self.local_artifact.serialize_for_create_artifact(
            workspace=workspace, work_request=work_request
        )

        self.assertEqual(serialized["work_request"], work_request)

    def test_validate_files_length(self):
        """_validate_files_length return the files."""
        files = {"package1.deb": Path(), "package2.deb": Path()}
        self.assertEqual(
            LocalArtifact._validate_files_length(files, len(files)), files
        )

    def test_validate_files_length_raise_error(self):
        """_validate_files_length raise ValueError."""
        files = {}
        expected = 1
        with self.assertRaisesRegex(
            ValueError, f"Expected number of files: {expected} Actual: 0"
        ):
            LocalArtifact._validate_files_length(files, expected)

    def test_serialize_for_create_with_expire_at(self):
        """serialize_for_create_artifact include the expire_at."""
        expire_at = datetime.now() + timedelta(days=1)

        serialized = self.local_artifact.serialize_for_create_artifact(
            workspace="workspac", expire_at=expire_at
        )

        self.assertEqual(serialized["expire_at"], expire_at.isoformat())


class PackageBuildLogTests(TestHelpersMixin, TestCase):
    """Tests for PackageBuildLog."""

    def setUp(self):
        """Set up for the tests."""
        self.artifact = PackageBuildLog(category="Debian")

        self.file = FileRequest(
            size=10, checksums={"sha256": "hash"}, type="file"
        )

        self.directory = self.create_temporary_directory()

        self.build_filename = "log.build"
        self.build_file = self.directory / self.build_filename
        self.build_file.write_text("A line of log")

    def test_create(self):
        """create() method return the expected PackageBuildLog."""
        source = "hello"
        version = "2.10-2"

        artifact = PackageBuildLog.create(
            file=self.directory / "log.build",
            source=source,
            version=version,
        )

        expected_data = {
            "source": source,
            "version": version,
        }
        expected_files = {self.build_filename: self.build_file}

        expected = PackageBuildLog(
            category=PackageBuildLog._category,
            files=expected_files,
            data=expected_data,
        )
        self.assertEqual(artifact, expected)
        self.assertIsInstance(artifact, PackageBuildLog)

    def test_validate_files_must_be_one_raise(self):
        """Raise ValueError: unexpected number of files."""
        with self.assertRaisesRegex(
            ValueError, "^Expected number of files: 1 Actual: 0$"
        ):
            PackageBuildLog.validate_files_length_is_one({})

    def test_validate_files_number_success(self):
        """files_number_must_be_one() return files."""
        files = {"log.build": self.file}

        self.assertEqual(
            PackageBuildLog.validate_files_length_is_one(files),
            files,
        )

    def test_validate_file_ends_with_build(self):
        """File ends with .build: files_must_end_in_build return files."""
        files = {"log.build": self.file}

        self.assertEqual(PackageBuildLog.file_must_end_in_build(files), files)

    def test_validate_file_ends_with_build_raise_value_error(self):
        """Raise ValueError: file does not end in .build."""
        file = self.create_temporary_file(suffix=".txt")

        expected_message = (
            fr"""^Valid file suffixes: \['\.build'\]. """
            fr"""Invalid filename: "{file.name}"$"""
        )

        with self.assertRaisesRegex(ValueError, expected_message):
            PackageBuildLog.file_must_end_in_build({file.name: file})

    def test_json_serializable(self):
        """Artifact returned by create is JSON serializable."""
        artifact = PackageBuildLog.create(
            file=self.build_file, source="hello", version="2.10-2"
        )

        # No exception is raised
        json.dumps(
            artifact.serialize_for_create_artifact(workspace="some-workspace")
        )


class BinaryUploadTests(TestHelpersMixin, TestCase):
    """Tests for BinaryUpload."""

    def setUp(self):
        """Set up test."""
        self.workspace = "workspace"

        self.artifact = BinaryUpload(category=BinaryUpload._category)

        self.file = FileRequest(
            size=10, checksums={"sha256": "hash"}, type="file"
        )

    def test_create(self):
        """create() return the correct object."""
        directory = self.create_temporary_directory()

        changes_filename = "python-network.changes"
        changes_file = directory / changes_filename

        # deb_file is not included in the BinaryUpload.files
        self.create_temporary_file(directory=directory)

        # output_file is included in the BinaryUpload.files
        output_file = self.create_temporary_file(directory=directory)
        changes = self.write_changes_file(changes_file, [output_file])

        artifact = BinaryUpload.create(
            changes_file=changes_file, exclude_files=set()
        )

        files = {changes_filename: changes_file, output_file.name: output_file}
        data = {
            "changes-fields": changes,
            "type": BinaryUpload._type,
        }

        expected = BinaryUpload(
            category=BinaryUpload._category,
            files=files,
            data=data,
        )

        self.assertEqual(artifact, expected)

    def test_create_skip_excluded_files(self):
        """BinaryUpload.create() does not add the excluded file."""
        changes_file = self.create_temporary_file(suffix=".changes")

        extra_file = self.create_temporary_file()

        self.write_changes_file(changes_file, files=[extra_file])

        binary_upload = BinaryUpload.create(
            changes_file=changes_file, exclude_files={extra_file}
        )

        files = {changes_file.name: changes_file}

        self.assertEqual(binary_upload.files, files)

    def test_create_no_files_in_changes(self):
        """BinaryUpload.create() return a package, .changes has no files."""
        changes_file = self.create_temporary_file(suffix=".changes")

        self.write_changes_file(changes_file, files=[])

        binary_upload = BinaryUpload.create(
            changes_file=changes_file, exclude_files=set()
        )

        files = {changes_file.name: changes_file}

        self.assertEqual(binary_upload.files, files)

    def test_type_must_be_in_data_raise(self):
        """type_must_be_in_data() raise ValueError."""
        with self.assertRaisesRegex(
            ValueError, '^Missing mandatory field in data: "type"$'
        ):
            BinaryUpload.type_must_be_in_data({})

    def test_type_must_be_in_data(self):
        """type_must_be_in_data() return the data."""
        data = {"type": "dpkg"}

        self.assertEqual(BinaryUpload.type_must_be_in_data(data), data)

    def test_json_serializable(self):
        """Artifact returned by create is JSON serializable."""
        directory = self.create_temporary_directory()
        output_file = self.create_temporary_file(directory=directory)
        changes = directory / "package.changes"
        self.write_changes_file(changes, files=[output_file])
        artifact = BinaryUpload.create(
            changes_file=changes, exclude_files=set()
        )

        json.dumps(
            artifact.serialize_for_create_artifact(workspace="some workspace")
        )

    def test_files_no_contain_changes_raise_value_error(self):
        """Raise ValueError: missing expected .changes file."""
        file = self.create_temporary_file()

        files = {file.name: file}

        expected_message = fr"No .changes in \['{file.name}'\]"
        with self.assertRaisesRegex(ValueError, expected_message):
            BinaryUpload.files_contain_changes(files)

    def test_files_contain_changes_return_files(self):
        """Return files: .changes found."""
        changes_file = self.create_temporary_file(suffix=".changes")

        self.write_changes_file(changes_file, files=[changes_file])

        files = {changes_file.name: changes_file}

        self.assertEqual(BinaryUpload.files_contain_changes(files), files)

    def test_files_contains_files_in_changes_call_implementation(self):
        """Test files_contains_files_in_dsc call the utils method."""
        result = {"a": "b"}
        patcher = mock.patch(
            "debusine.artifacts.local_artifact.files_in_meta_file_match_files"
        )
        mocked = patcher.start()
        mocked.return_value = result
        self.addCleanup(patcher.stop)

        files = {}

        self.assertEqual(
            BinaryUpload.files_contains_files_in_changes(files), result
        )

        mocked.assert_called_with(
            ".changes",
            deb822.Changes,
            files,
            ignore_suffixes_metadata=(".deb", ".udeb"),
        )


class Deb822DictToDictTests(TestCase):
    """Tests for deb822dict_to_dict function."""

    def assert_deb822dict_return_correct_value(self, value, expected):
        """Use deb822dict_to_dict(value) and check compare with expected."""
        actual = deb822dict_to_dict(value)

        self.assertEqual(actual, expected)
        json.dumps(actual)

    def test_dict(self):
        """Python dictionary is returned as it is."""
        to_convert = {"a": "b"}
        expected = {"a": "b"}
        self.assert_deb822dict_return_correct_value(to_convert, expected)

    def test_deb822_dict(self):
        """Deb822Dict is converted to a Python dict."""
        to_convert = deb822.Deb822Dict({"a": "b"})
        expected = {"a": "b"}
        self.assert_deb822dict_return_correct_value(to_convert, expected)

    def test_list(self):
        """A list with a Deb822Dict is returned as expected."""
        to_convert = ["key", deb822.Deb822Dict({"a": "b"})]
        expected = ["key", {"a": "b"}]
        self.assert_deb822dict_return_correct_value(to_convert, expected)

    def test_str(self):
        """A string is returned as it is."""
        to_convert = deb822dict_to_dict("all")
        expected = "all"
        self.assert_deb822dict_return_correct_value(to_convert, expected)


class BinaryPackagesTests(TestHelpersMixin, TestCase):
    """Tests for BinaryPackages."""

    def test_create(self):
        """create() return the expected package: correct data and files."""
        srcpkg_name = "hello"
        srcpkg_version = "2.10-2"
        version = "1.1.1"
        architecture = "amd64"
        packages = ["hello-traditional", "hello-dbg"]
        files = [
            self.create_temporary_file(
                prefix="debusine-artifact-test", suffix=".deb"
            ),
            self.create_temporary_file(
                prefix="debusine-artifact-test", suffix=".udeb"
            ),
        ]

        package = BinaryPackages.create(
            srcpkg_name=srcpkg_name,
            srcpkg_version=srcpkg_version,
            version=version,
            architecture=architecture,
            packages=packages,
            files=files,
        )

        expected = BinaryPackages(category=BinaryPackages._category)
        expected.data = {
            "srcpkg-name": srcpkg_name,
            "srcpkg-version": srcpkg_version,
            "version": version,
            "architecture": architecture,
            "packages": packages,
        }
        expected.files = {
            files[0].name: files[0],
            files[1].name: files[1],
        }

        self.assertEqual(package, expected)

    def test_files_must_end_in_deb_or_udeb(self):
        """files_must_end_in_deb_or_udeb() return the files."""
        files = {
            "hello_2.10-2_amd64.deb": None,
            "hello_2.10-2_amd64.udeb": None,
        }
        self.assertEqual(
            BinaryPackages.files_must_end_in_deb_or_udeb(files),
            files,
        )

    def test_files_must_end_in_deb_raise_value_error(self):
        """files_must_end_in_deb_or_udeb() raise ValueError."""
        filename = "README.txt"
        files = {filename: None}
        with self.assertRaisesRegex(
            ValueError,
            rf"^Valid file suffixes: \['.deb', '.udeb'\]. "
            rf"Invalid filename: \"{filename}\"$",
        ):
            BinaryPackages.files_must_end_in_deb_or_udeb(files)

    def test_files_more_than_zero(self):
        """files_more_than_zero() return the files."""
        files = {"hello.deb": None, "hello2.deb": None}

        self.assertEqual(BinaryPackages.files_more_than_zero(files), files)

    def test_files_more_than_zero_raise_value_error(self):
        """files_more_than_zero() raise ValueError: zero files."""
        with self.assertRaisesRegex(ValueError, "Must have at least one file"):
            BinaryPackages.files_more_than_zero({})

    def test_json_serializable(self):
        """Artifact returned by create is JSON serializable."""
        file = self.create_temporary_file(suffix=".deb")
        artifact = BinaryPackages.create(
            srcpkg_name="hello",
            srcpkg_version="2.10-2",
            version="2.10-2",
            architecture="amd64",
            packages=["hello_amd64.deb"],
            files=[file],
        )

        # This is to verity that serialize_for_create_artifact() is
        # not returning some object that might raise an exception on
        # json.dumps. E.g. if it returned (as a value of the dictionary) a set()
        json.dumps(
            artifact.serialize_for_create_artifact(workspace="some workspace")
        )


class WorkRequestDebugLogsTests(TestHelpersMixin, TestCase):
    """Tests for WorkRequestDebugLogs class."""

    def test_create(self):
        """Test create(): return instance with expected files and category."""
        log_file_1 = self.create_temporary_file()
        log_file_2 = self.create_temporary_file()
        package = WorkRequestDebugLogs.create(files=[log_file_1, log_file_2])

        expected = LocalArtifact(category=WorkRequestDebugLogs._category)
        expected.files = {
            log_file_1.name: log_file_1,
            log_file_2.name: log_file_2,
        }

        self.assertEqual(package, expected)


class SourcePackageTests(TestHelpersMixin, TestCase):
    """Tests for SourcePackage class."""

    def test_create(self):
        """Test create(): return expected instance."""
        file = self.create_temporary_file()
        dsc_file = self.create_temporary_file(suffix=".dsc")

        dsc_fields = self.write_dsc_example_file(dsc_file)

        name = dsc_fields["Source"]
        version = dsc_fields["Version"]

        expected = LocalArtifact(category=SourcePackage._category)
        expected.files = {file.name: file, dsc_file.name: dsc_file}
        expected.data = {
            "name": name,
            "version": version,
            "type": "dpkg",
            "dsc-fields": dsc_fields,
        }

        package = SourcePackage.create(
            name=name, version=version, files=[file, dsc_file]
        )

        self.assertEqual(package, expected)

    def test_files_no_contain_dsc_raise_value_error(self):
        """Raise ValueError: missing expected .dsc file."""
        file = self.create_temporary_file()

        files = {file.name: file}

        expected_message = "^Must be one .dsc file. There are: 0$"
        with self.assertRaisesRegex(ValueError, expected_message):
            SourcePackage.files_contain_one_dsc(
                files,
            )

    def test_files_contains_dsc_return_files(self):
        """Return all files: .dsc is in the files."""
        file = self.create_temporary_file(suffix=".dsc")

        files = {file.name: file}

        self.assertEqual(
            SourcePackage.files_contain_one_dsc(
                files,
            ),
            files,
        )

    def test_files_contains_files_in_dsc_call_implementation(self):
        """Test files_contains_files_in_dsc call the utils method."""
        result = {"a": "b"}
        patcher = mock.patch(
            "debusine.artifacts.local_artifact.files_in_meta_file_match_files"
        )
        mocked = patcher.start()
        mocked.return_value = result
        self.addCleanup(patcher.stop)

        files = {}

        self.assertEqual(
            SourcePackage.files_contains_files_in_dsc(files), result
        )

        mocked.assert_called_with(".dsc", deb822.Dsc, files)


class LintianTests(TestHelpersMixin, TestCase):
    """Tests for the LintianArtifact."""

    @classmethod
    def setUpClass(cls):
        """Set up common data for the tests."""
        cls.output_content = (
            b"W: python-ping3 source: missing-license-"
            b"paragraph-in-dep5-copyright gpl-3 [debian/copyright:33]\n"
            b"P: python-ping3 source: maintainer-manual-page [debian/ping3.1]\n"
        )
        cls.summary_content = json.dumps({"warning": 2, "pedantic": 1}).encode(
            "utf-8"
        )
        cls.analysis_content = json.dumps(
            {
                "tags": [
                    {
                        "package": "cynthiune.app",
                        "severity": "warning",
                        "tag": "vcs-obsolete",
                        "note": "",
                        "pointer": "",
                        "explanation": "",
                        "comment": "",
                    },
                    {
                        "package": "binutils",
                        "severity": "error",
                        "tag": "license-problem-convert-utf-code",
                        "note": "Cannot ls",
                        "pointer": "src/ls.c",
                        "explanation": "",
                        "comment": "",
                    },
                ]
            }
        ).encode("utf-8")

    def test_create(self):
        """Test create(): return expected class with the files."""
        output = self.create_temporary_file(
            suffix=".txt", contents=self.output_content
        )
        analysis = self.create_temporary_file(
            suffix=".json", contents=self.analysis_content
        )

        package = LintianArtifact.create(
            lintian_output=output, analysis=analysis
        )

        self.assertEqual(package._category, LintianArtifact._category)
        self.assertEqual(
            package.files,
            {
                "lintian.txt": output,
                "analysis.json": analysis,
            },
        )

    def test_validate_file_analysis_is_json_valid(self):
        """Test validate_file_analysis_is_json: return files."""
        summary = self.create_temporary_file(contents=self.summary_content)
        analysis = self.create_temporary_file(contents=b"{}")
        files = {"summary.json": summary, "analysis.json": analysis}

        self.assertEqual(
            LintianArtifact._validate_file_analysis_is_json(files),
            files,
        )

    def test_validate_file_analysis_json_raise_value_error(self):
        """Test validate_file_analysis_is_json: raise ValueError."""
        analysis = self.create_temporary_file(contents=b":")  # invalid

        msg = "^analysis.json is not valid JSON:"
        with self.assertRaisesRegex(ValueError, msg):
            LintianArtifact._validate_file_analysis_is_json(
                {"analysis.json": analysis}
            )

    def test_validate_required_files_valid(self):
        """Test validate_required_files: is valid. return files."""
        files = {"analysis.json": "", "lintian.txt": ""}

        self.assertEqual(LintianArtifact._validate_required_files(files), files)

    def test_validate_required_files_missing_file_raise_value_error(self):
        """Test validate_required_files: not valid, raise ValueError."""
        msg = r"^Files required: " r"\['analysis.json', 'lintian.txt'\]$"

        with self.assertRaisesRegex(ValueError, msg):
            LintianArtifact._validate_required_files({"summary.json": ""})


class AutopkgtestArtifactTests(TestHelpersMixin, TestCase):
    """Tests for AutopkgtestArtifact class."""

    def test_create(self):
        """Test _create()."""
        directory = self.create_temporary_directory()

        # Create files in directory and subdirectories: to be included in
        # the package
        (summary_file := directory / "summary").write_text("summary content")

        (subdir := directory / "subdir").mkdir()
        (file_in_subdir := subdir / "some-file.txt").write_text("test")

        (binaries_subdir := directory / "binaries").mkdir()

        # Files in "binaries/" are not part of the artifact
        (binaries_subdir / "log.txt").write_text("log file")
        (binaries_subdir / "pkg.deb").write_text("deb")

        package = AutopkgtestArtifact.create(directory)

        self.assertIsInstance(package, AutopkgtestArtifact)

        self.assertEqual(
            package.files,
            {
                "subdir/some-file.txt": file_in_subdir,
                "summary": summary_file,
            },
        )
