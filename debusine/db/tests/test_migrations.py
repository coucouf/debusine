# Copyright 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Unit tests for the migrations."""

from django.test import TestCase

from debusine.db.models import (
    FileStore,
    default_file_store,
    default_workspace,
)


class MigrationTests(TestCase):
    """Test migrations."""

    def test_default_store_created(self):
        """Assert Default FileStore has been created."""
        default_file_store = FileStore.default()

        self.assertEqual(
            default_file_store.backend, FileStore.BackendChoices.LOCAL
        )

        self.assertEqual(default_file_store.configuration, {})

    def test_system_workspace_created(self):
        """Assert System Workspace has been created."""
        workspace = default_workspace()
        self.assertEqual(workspace.default_file_store, default_file_store())
