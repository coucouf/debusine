"""
The settings in this file are tailored overrides for running in production.

When running in production, selected.py should point to this file.
"""

# PostgreSQL should be used in production
from .db_postgresql import DATABASES  # noqa: F401

# Use paths from the package
from .pkg_paths import *  # noqa: F401, F403, I202
from ..utils import read_secret_key

# Read the SECRET_KEY from the file
SECRET_KEY = read_secret_key(f"{DEBUSINE_DATA_PATH}/key")  # noqa: F405
