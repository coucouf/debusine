# Copyright 2020-2023 Enrico Zini <enrico@debian.org>
# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""
Logic to authenticate a request using signon Providers.

If SIGNON_AUTO_CREATE_USER is set to True, a User is created when external
providers authenticate a valid user with no matching local user.

If SIGNON_AUTO_BIND is set to True, and we are authenticated with an external
identity provider which matches a local user, and we are authenticated also
with other external providers that do not match any local user, auto-associate
the latter identities to the current user.
"""
import functools
import logging
from typing import Iterable, Optional, TYPE_CHECKING, Tuple

import django.http
from django.conf import settings
from django.contrib import auth
from django.contrib.auth import load_backend

from debusine.db.models import Identity

from . import providers
from .auth import SignonAuthBackend
from .utils import split_full_name

if TYPE_CHECKING:  # pragma: no cover
    from django.contrib.auth.models import AbstractUser

log = logging.getLogger("debusine.server.signon")


class Signon:
    """
    Backend used to interact with external authentication providers.

    This is setup by SignonMiddleware as request.signon.

    The constructor needs to be as lightweight as possible, as it is called on
    every request. Everything else is loaded only when needed.
    """

    def __init__(self, request: django.http.HttpRequest):
        """Create a Signon object for a request."""
        self.request = request
        self.providers = getattr(settings, "SIGNON_PROVIDERS", ())

    def status(
        self,
    ) -> Iterable[Tuple[providers.BoundProvider, Optional[Identity]]]:
        """
        Query the status of remote authentication providers.

        :returns: an iterable of ``(bound_provider, identity | None)``
        """
        for provider in self.providers:
            bound = provider.bind(self.request)
            identity = self.identities.get(provider.name)
            yield bound, identity

    @functools.cached_property
    def identities(self) -> dict[str, Identity]:
        """Lazily populate self.identities."""
        return self._compute_identities()

    def logout_identities(self):
        """Deactivate all active external identities."""
        for provider in self.providers:
            bound = provider.bind(self.request)
            bound.logout()
        self.update_authentication()

    def _compute_identities(self) -> dict[str, Identity]:
        """
        Instantiate valid Identity entries for this request.

        Delegate Provider objects with looking up valid Identity objects from
        the current request.
        """
        identities = {}

        for provider in self.providers:
            pk = self.request.session.get(f"signon_identity_{provider.name}")
            if pk is None:
                continue

            try:
                identity = Identity.objects.get(pk=pk, issuer=provider.name)
            except Identity.DoesNotExist:
                # If the session has a broken Identity ID, remove it
                del self.request.session[f"signon_identity_{provider.name}"]
                continue

            identities[provider.name] = identity

        return identities

    def update_authentication(self):
        """
        Update authentication status and request.user.

        This instantiates `request.signon.identities` from the session, and
        updates request.user according to currently active identities.

        Call this method whenever identities get activated or deactivated.
        """
        # Refresh the instantiated identities
        self.identities = self._compute_identities()

        # If no identity is logged in, remove any existing authenticated
        # signon user and stop processing
        if not self.identities:
            log.debug("no identities found")
            if self.request.user.is_authenticated:
                self._remove_invalid_signon_user()
            return

        # Infer a user from the active identities
        user = self._find_bound_user()
        log.debug("inferred user: %s", user)

        # If we cannot map active identities to a user, then remove any
        # existing user that was authenticated by us
        if user is None and self.request.user.is_authenticated:
            log.debug(
                "existing user %s but no valid user from remote identities",
                user,
            )
            self._remove_invalid_signon_user()

        # If the inferred user does not match the current user, and the current
        # user was authenticated by us, log them out
        if (
            user is not None
            and self.request.user.is_authenticated
            and self.request.user.pk != user.pk
        ):
            log.debug(
                "inferred user %s not matching existing user %s",
                user,
                self.request.user,
            )
            self._remove_invalid_signon_user()

        # If we have active identities that are consistently bound to the same
        # user (ignoring the unbound ones), then we can proceed to log in that
        # user
        if user is not None and not self.request.user.is_authenticated:
            log.debug("logging in user %s", user)
            auth.login(
                self.request,
                user,
                backend="debusine.server.signon.auth.SignonAuthBackend",
            )

        if (
            user is None
            and not self.request.user.is_authenticated
            and getattr(settings, "SIGNON_AUTO_CREATE_USER", False)
            and all(ident.user is None for ident in self.identities.values())
        ):
            if (user := self._auto_create_user()) is not None:
                log.debug("logging in autocreated user %s", user)
                self.request.user = user
                auth.login(
                    self.request,
                    user,
                    backend="debusine.server.signon.auth.SignonAuthBackend",
                )

        # If we have a valid user, auto_bind all valid identities to it
        if self.request.user.is_authenticated and getattr(
            settings, "SIGNON_AUTO_BIND", False
        ):
            log.debug("performing auto bind")
            self._auto_bind()

    def _remove_invalid_signon_user(self):
        """
        Log out an externally authenticated user.

        This is used to invalidate credentials in case a consistency check
        failed between active identities.

        Log out only happens if the user was authenticated via SignonMiddleware
        """
        try:
            stored_backend = load_backend(
                self.request.session.get(auth.BACKEND_SESSION_KEY, '')
            )
        except ImportError:
            # backend failed to load
            auth.logout(self.request)
        else:
            if isinstance(stored_backend, SignonAuthBackend):
                auth.logout(self.request)

    def _find_bound_user(self):
        """
        Check if active identities are consistently bound to a user.

        If request.signon.identities has some bound identities, return the
        user bound to them.

        Return None if there are identities bound to different people.
        """
        user = None
        for identity in self.identities.values():
            # Skip unbound identities
            if identity.user_id is None:
                continue

            # Make sure that all the bound identities point at the same user
            if user is None:
                user = identity.user
            elif user.pk != identity.user_id:
                log.error(
                    "Conflicting user mapping:"
                    " identities (%s) map to at least %s and %s",
                    ", ".join(str(x) for x in self.identities.values()),
                    user,
                    identity.user,
                )
                return None

        if user is None or not user.is_active:
            return None

        return user

    def _auto_create_user(self) -> Optional["AbstractUser"]:
        """
        Create a user model from active identities.

        Automatically create a user from the first active identity, and bind
        the other active identities to it.

        This assumes that request.signon.identities contains at least one
        active identity, and that all active identities are unbound
        """
        # User = auth.get_user_model()
        user: Optional[AbstractUser] = None

        # Scan identities looking for one from which a valid user can be
        # instantiated
        for identity in self.identities.values():
            user = self._get_or_create_user_from_identity(identity)
            if user is not None:
                log.info("%s: auto created from identity %s", user, identity)
                break

        # Stop here if no user could be created
        if user is None:
            return None

        # Associate the new user to all active identities
        # TODO: discuss this behaviour as part of #201
        for identity in self.identities.values():
            identity.user = user
            log.info("%s: auto bound to identity %s", user, identity)
            identity.save()

        return user

    def _auto_bind(self):
        """Auto bind unbound identities to the user currently logged in."""
        for identity in self.identities.values():
            if (
                identity.user is not None
                and identity.user.pk == self.request.user.pk
            ):
                continue

            log.info("%s: auto associated to %s", self.request.user, identity)
            identity.user = self.request.user
            identity.save()

    @staticmethod
    def _get_or_create_user_from_identity(
        identity: Identity,
    ) -> Optional["AbstractUser"]:
        """Lookup or create a user from the data in an Identity."""
        # Work only with verified emails
        if not identity.claims.get("email_verified", False):
            log.warning("identity %s does not have a verified email", identity)
            return None

        # Lookup an existing user
        User = auth.get_user_model()
        try:
            return User.objects.get(email=identity.claims["email"])
        except User.DoesNotExist:
            pass

        # Create a new user
        first_name, last_name = split_full_name(identity.claims["name"])
        return User.objects.create_user(
            username=identity.claims["email"],
            first_name=first_name,
            last_name=last_name,
            email=identity.claims["email"],
        )
