# Copyright 2020-2023 Enrico Zini <enrico@debian.org>
# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Test of the backend for signon authentication using external providers."""

import django.contrib.sessions.backends.base
from django.contrib import auth
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory, TestCase, override_settings

from debusine.db.models import Identity
from debusine.server.signon import providers
from debusine.server.signon.auth import SignonAuthBackend
from debusine.server.signon.signon import Signon


class MockSession(django.contrib.sessions.backends.base.SessionBase):
    """In-memory session with no persistence, used for tests."""

    # TODO: is there a more standard way to provide a mock session?
    # Would SessionStore be enough?

    # def exists(self):
    #     """Mock SessionBase API endpoint."""
    #     return True

    def create(self):
        """Mock SessionBase API endpoint."""
        pass

    # def save(self):
    #     """Mock SessionBase API endpoint."""
    #     pass

    def delete(self):
        """Mock SessionBase API endpoint."""
        pass

    # def load(self):
    #     """Mock SessionBase API endpoint."""
    #     pass

    # def clear_expired(self):
    #     """Mock SessionBase API endpoint."""
    #     pass


@override_settings(
    SIGNON_PROVIDERS=[
        providers.Provider(name="debsso", label="sso.debian.org"),
        providers.Provider(name="salsa", label="Salsa"),
    ]
)
class TestAuthentication(TestCase):
    """Test Signon."""

    # Remove from python 3.10+
    if not hasattr(TestCase, "assertNoLogs"):  # pragma: no cover
        import logging
        from unittest._log import _AssertLogsContext

        class _AssertNoLogsContext(_AssertLogsContext):
            """A context manager for polyfilling assertNoLogs()."""

            def __exit__(self, exc_type, *args):
                self.logger.handlers = self.old_handlers
                self.logger.propagate = self.old_propagate
                self.logger.setLevel(self.old_level)

                if exc_type is not None:
                    # let unexpected exceptions pass through
                    return False

                # assertNoLogs
                if len(self.watcher.records) > 0:
                    self._raiseFailure(
                        "Unexpected logs found: {!r}".format(
                            self.watcher.output
                        )
                    )

        def assertNoLogs(self, logger=None, level=None):
            """Polyfill assertNoLogs for older pythons."""
            return TestAuthentication._AssertNoLogsContext(self, logger, level)

    def setUp(self):
        """Provide a mock unauthenticated request for tests."""
        super().setUp()
        self.factory = RequestFactory()
        self.request = self.factory.get("/")
        self.request.session = MockSession()
        self.request.user = AnonymousUser()
        self.request.signon = Signon(self.request)

    def _signon(self, *args: Identity):
        """
        Perform signon with external identities defined by kwargs.

        kwargs maps provider names to Identity objects that will be mocked as
        authenticated by external providers
        """
        for name in list(self.request.session.keys()):
            if name.startswith("signon_identity_"):
                del self.request.session[name]

        for identity in args:
            self.request.session[
                f"signon_identity_{identity.issuer}"
            ] = identity.pk
        self.request.signon.update_authentication()

    def test_no_active_identities(self):
        """No active external identities leave request unauthenticated."""
        self._signon()
        self.assertEqual(self.request.signon.identities, {})
        self.assertFalse(self.request.user.is_authenticated)

        status = list(self.request.signon.status())
        self.assertEqual(len(status), 2)
        self.assertEqual(status[0][0].name, "debsso")
        self.assertIsNone(status[0][1])
        self.assertEqual(status[1][0].name, "salsa")
        self.assertIsNone(status[1][1])

    def test_one_active_unbound_identity(self):
        """One active but unbound identity leave request unauthenticated."""
        identity = Identity.objects.create(
            issuer="debsso",
            subject="user@debian.org",
        )
        with self.assertNoLogs("debusine.server.signon"):
            self._signon(identity)

        self.assertEqual(
            self.request.signon.identities,
            {
                "debsso": identity,
            },
        )
        self.assertFalse(self.request.user.is_authenticated)

        status = list(self.request.signon.status())
        self.assertEqual(len(status), 2)
        self.assertEqual(status[0][0].name, "debsso")
        self.assertEqual(status[0][1], identity)
        self.assertEqual(status[1][0].name, "salsa")
        self.assertIsNone(status[1][1])

    def test_one_active_bound_identity(self):
        """One active bound identity is enough to authenticate."""
        user = get_user_model().objects.create_user("test")
        identity = Identity.objects.create(
            user=user,
            issuer="debsso",
            subject="user@debian.org",
        )
        with self.assertNoLogs("debusine.server.signon"):
            self._signon(identity)

        self.assertEqual(
            self.request.signon.identities,
            {
                "debsso": identity,
            },
        )
        self.assertEqual(self.request.user, user)
        self.assertIsInstance(
            auth.load_backend(
                self.request.session.get(auth.BACKEND_SESSION_KEY)
            ),
            SignonAuthBackend,
        )

    def test_one_active_missing_identity(self):
        """One active identity not present in the DB is deactivated."""
        self.request.session["signon_identity_salsa"] = 1
        self.request.signon.update_authentication()

        self.assertEqual(self.request.signon.identities, {})
        self.assertFalse(self.request.user.is_authenticated)

        self.assertIsNone(self.request.session.get("signon_identity_salsa"))

        status = list(self.request.signon.status())
        self.assertEqual(len(status), 2)
        self.assertEqual(status[0][0].name, "debsso")
        self.assertIsNone(status[0][1])
        self.assertEqual(status[1][0].name, "salsa")
        self.assertIsNone(status[1][1])

    def test_bound_and_unbound_identities(self):
        """One active bound identity and one active unbound, do authenticate."""
        user = get_user_model().objects.create_user("test")
        ident1 = Identity.objects.create(
            user=user,
            issuer="debsso",
            subject="user1@debian.org",
        )
        ident2 = Identity.objects.create(
            issuer="salsa",
            subject="user2@debian.org",
        )
        with self.assertNoLogs("debusine.server.signon"):
            self._signon(ident1, ident2)

        self.assertEqual(
            self.request.signon.identities,
            {
                "debsso": ident1,
                "salsa": ident2,
            },
        )
        self.assertEqual(self.request.user, user)

        # SIGNON_AUTO_BIND is False, so ident2 is still unbound
        ident2.refresh_from_db()
        self.assertIsNone(ident2.user)

    def test_logout_identities_logs_our_user_out(self):
        """logout_identities() deactivates identities."""
        # Log in using an external identity
        user = get_user_model().objects.create_user("test")
        ident1 = Identity.objects.create(
            user=user,
            issuer="debsso",
            subject="user1@debian.org",
        )
        with self.assertNoLogs("debusine.server.signon"):
            self._signon(ident1)
        self.assertEqual(self.request.user, user)

        # Log out identities
        self.request.signon.logout_identities()
        self.assertEqual(self.request.signon.identities, {})

        # Since the user was logged in by external identities, it's now logged
        # out
        self.assertFalse(self.request.user.is_authenticated)

    def test_logout_identities_keep_user_from_other_backends(self):
        """logout_identities() deactivates identities."""
        # Log in using ModelBackend
        user = get_user_model().objects.create_user("test")
        self.request.user = user
        self.request.session[
            auth.BACKEND_SESSION_KEY
        ] = "django.contrib.auth.backends.ModelBackend"

        # Also activate an identity mapped to user
        ident1 = Identity.objects.create(
            user=user,
            issuer="debsso",
            subject="user1@debian.org",
        )
        with self.assertNoLogs("debusine.server.signon"):
            self._signon(ident1)
        self.assertEqual(self.request.user, user)

        # Log out identities
        self.request.signon.logout_identities()
        self.assertEqual(self.request.signon.identities, {})

        # Since the user was logged in by another auth backend, it's still
        # logged in
        self.assertTrue(self.request.user.is_authenticated)
        self.assertEqual(self.request.user, user)

    def test_conflicting_active_bound_identities(self):
        """Two active identities bound to different users don't authenticate."""
        # Multiple active bound identities pointing to different users cause
        # failure to authenticate
        user1 = get_user_model().objects.create_user(
            "test1", email="test1@example.org"
        )
        user2 = get_user_model().objects.create_user(
            "test2", email="test2@example.org"
        )
        ident1 = Identity.objects.create(
            user=user1,
            issuer="debsso",
            subject="user1@debian.org",
        )
        ident2 = Identity.objects.create(
            user=user2,
            issuer="salsa",
            subject="user2@debian.org",
        )
        with self.assertLogs("debusine.server.signon") as log:
            self._signon(ident1, ident2)

        self.assertEqual(
            self.request.signon.identities,
            {
                "debsso": ident1,
                "salsa": ident2,
            },
        )
        self.assertFalse(self.request.user.is_authenticated)

        self.assertEqual(
            log.output,
            [
                "ERROR:debusine.server.signon:"
                f"Conflicting user mapping: identities ({ident1}, {ident2})"
                f" map to at least {user1} and {user2}"
            ],
        )

    def test_aligned_active_bound_identities(self):
        """Two active identities bound to same users do authenticate."""
        # Multiple active bound identities pointing to the same user
        # authenticate successfully
        user = get_user_model().objects.create_user(
            "test", email="test@example.org"
        )
        ident1 = Identity.objects.create(
            user=user,
            issuer="debsso",
            subject="user1@debian.org",
        )
        ident2 = Identity.objects.create(
            user=user,
            issuer="salsa",
            subject="user2@debian.org",
        )

        with self.assertNoLogs("debusine.server.signon"):
            self._signon(ident1, ident2)

        self.assertEqual(
            self.request.signon.identities,
            {
                "debsso": ident1,
                "salsa": ident2,
            },
        )
        self.assertEqual(self.request.user, user)

    def test_keep_internal_user_on_identity_misaligned(self):
        """Conflict between the current internal user and a new external one."""
        user1 = get_user_model().objects.create_user(
            "test1", email="test1@example.org"
        )
        user2 = get_user_model().objects.create_user(
            "test2", email="test2@example.org"
        )
        ident = Identity.objects.create(
            user=user2,
            issuer="debsso",
            subject="test2@debian.org",
        )

        # Simulate a valid existing login from a different auth backend
        self.request.user = user1
        self.request.session[
            auth.BACKEND_SESSION_KEY
        ] = "django.contrib.auth.backends.ModelBackend"

        # Now activate a conflicting identity
        self.request.session[f"signon_identity_{ident.issuer}"] = ident.pk
        self.request.signon.update_authentication()

        # The previous user is still logged in
        self.assertTrue(self.request.user.is_authenticated)
        self.assertEqual(self.request.user, user1)

        # The identity is still active
        self.assertEqual(self.request.signon.identities, {"debsso": ident})

    def test_remove_external_user_on_new_identity(self):
        """Conflict between the current external user and a new one."""
        # Log in with one identity
        user1 = get_user_model().objects.create_user(
            "test1", email="test1@example.org"
        )
        ident1 = Identity.objects.create(
            user=user1,
            issuer="debsso",
            subject="test1@debian.org",
        )
        self._signon(ident1)
        self.assertTrue(self.request.user.is_authenticated)
        self.assertEqual(self.request.user, user1)

        # The previous identity is deactivated, and a new conflicting identity
        # is activated
        user2 = get_user_model().objects.create_user(
            "test2", email="test2@example.org"
        )
        ident2 = Identity.objects.create(
            user=user2,
            issuer="salsa",
            subject="test2@debian.org",
        )
        self._signon(ident2)
        self.assertEqual(
            self.request.signon.identities,
            {
                "salsa": ident2,
            },
        )

        # The previous user is replaced
        self.assertTrue(self.request.user.is_authenticated)
        self.assertEqual(self.request.user, user2)

    def test_remove_external_user_on_identity_conflict(self):
        """Conflict between the current external user and a new one."""
        # Log in with one identity
        user1 = get_user_model().objects.create_user(
            "test1", email="test1@example.org"
        )
        ident1 = Identity.objects.create(
            user=user1,
            issuer="debsso",
            subject="test1@debian.org",
        )
        self._signon(ident1)
        self.assertTrue(self.request.user.is_authenticated)
        self.assertEqual(self.request.user, user1)

        # A new conflicting identity is activated
        user2 = get_user_model().objects.create_user(
            "test2", email="test2@example.org"
        )
        ident2 = Identity.objects.create(
            user=user2,
            issuer="salsa",
            subject="test2@debian.org",
        )
        self._signon(ident1, ident2)

        # The conflict cannot be resolved, and the user is logged out
        self.assertFalse(self.request.user.is_authenticated)

        # Both identities are still active
        self.assertEqual(
            self.request.signon.identities,
            {
                "debsso": ident1,
                "salsa": ident2,
            },
        )

    def test_auth_fails_on_identity_conflict(self):
        """Conflict between the current external user and a new one."""
        # Log in with one identity
        user1 = get_user_model().objects.create_user(
            "test1", email="test1@example.org"
        )
        ident1 = Identity.objects.create(
            user=user1,
            issuer="debsso",
            subject="test1@debian.org",
        )

        # A new conflicting identity is activated
        user2 = get_user_model().objects.create_user(
            "test2", email="test2@example.org"
        )
        ident2 = Identity.objects.create(
            user=user2,
            issuer="salsa",
            subject="test2@debian.org",
        )

        self._signon(ident1, ident2)

        # No user has been logged in
        self.assertFalse(self.request.user.is_authenticated)

        # Both identities are still active
        self.assertEqual(
            self.request.signon.identities,
            {
                "debsso": ident1,
                "salsa": ident2,
            },
        )

    def test_remove_user_on_identity_deactivated(self):
        """If the external identity is logged out, user is logged out."""
        user = get_user_model().objects.create_user(
            "test", email="test@example.org"
        )
        ident = Identity.objects.create(
            user=user,
            issuer="debsso",
            subject="user@debian.org",
        )

        # Authenticate successfully with one active identity
        with self.assertNoLogs("debusine.server.signon"):
            self._signon(ident)

        self.assertTrue(self.request.user.is_authenticated)
        self.assertEqual(self.request.signon.identities, {"debsso": ident})

        # That identity is somehow logged out
        with self.assertNoLogs("debusine.server.signon"):
            self._signon()

        # The debusine user is now also logged out
        self.assertFalse(self.request.user.is_authenticated)

    def test_remove_user_on_identity_conflict(self):
        """Logging into a second conflicting external ident logs user out."""
        user1 = get_user_model().objects.create_user(
            "test1", email="test1@example.org"
        )
        user2 = get_user_model().objects.create_user(
            "test2", email="test2@example.org"
        )
        ident1 = Identity.objects.create(
            user=user1,
            issuer="debsso",
            subject="user1@debian.org",
        )
        ident2 = Identity.objects.create(
            user=user2,
            issuer="salsa",
            subject="user2@debian.org",
        )

        # We were authenticated as one user
        self.request.user = user1

        # Add a conflicting external identity provider: the conflict should log
        # us out
        with self.assertLogs("debusine.server.signon") as log:
            self._signon(ident1, ident2)

        self.assertFalse(self.request.user.is_authenticated)

        self.assertEqual(
            log.output,
            [
                "ERROR:debusine.server.signon:"
                f"Conflicting user mapping: identities ({ident1}, {ident2})"
                f" map to at least {user1} and {user2}"
            ],
        )

    @override_settings(SIGNON_AUTO_BIND=True)
    def test_auto_bind(self):
        """Auto bind an unbound identity if user is known."""
        user = get_user_model().objects.create_user(
            "test1", email="test1@example.org"
        )
        ident = Identity.objects.create(
            issuer="debsso",
            subject="user1@debian.org",
        )

        # Simulate a valid existing login from a different auth backend
        self.request.user = user
        self.request.session[
            auth.BACKEND_SESSION_KEY
        ] = "django.contrib.auth.backends.ModelBackend"

        # Try to log in with the unbound identity
        with self.assertLogs("debusine.server.signon") as log:
            self._signon(ident)

        self.assertEqual(self.request.user, user)
        self.assertEqual(
            self.request.signon.identities,
            {
                "debsso": ident,
            },
        )

        self.assertEqual(
            log.output,
            [
                "INFO:debusine.server.signon:"
                f"{user}: auto associated to {ident}",
            ],
        )

        ident.refresh_from_db()
        self.assertEqual(ident.user, user)

    @override_settings(SIGNON_AUTO_BIND=True)
    def test_auto_bind_some_are_bound(self):
        """Auto bind leaves correctly bound identities alone."""
        user = get_user_model().objects.create_user(
            "test1", email="test1@example.org"
        )
        ident1 = Identity.objects.create(
            user=user,
            issuer="debsso",
            subject="user1@debian.org",
        )
        ident2 = Identity.objects.create(
            issuer="salsa",
            subject="user2@debian.org",
        )

        # Simulate a valid existing login from a different auth backend
        self.request.user = user
        self.request.session[
            auth.BACKEND_SESSION_KEY
        ] = "django.contrib.auth.backends.ModelBackend"

        # Try to log in with both identities
        with self.assertLogs("debusine.server.signon") as log:
            self._signon(ident1, ident2)

        self.assertEqual(self.request.user, user)
        self.assertEqual(
            self.request.signon.identities,
            {
                "debsso": ident1,
                "salsa": ident2,
            },
        )

        self.assertEqual(
            log.output,
            [
                "INFO:debusine.server.signon:"
                f"{user}: auto associated to {ident2}",
            ],
        )

        ident1.refresh_from_db()
        self.assertEqual(ident1.user, user)
        ident2.refresh_from_db()
        self.assertEqual(ident2.user, user)

    @override_settings(SIGNON_AUTO_CREATE_USER=True)
    def test_auto_create_user(self):
        """Auto create and bind Django user for a new identity."""
        ident = Identity.objects.create(
            issuer="salsa",
            subject="user@debian.org",
            claims={
                "name": "Test User",
                "email": "test@example.org",
                "email_verified": True,
            },
        )

        with self.assertLogs("debusine.server.signon") as log:
            self._signon(ident)

        ident.refresh_from_db()
        self.assertIsNotNone(ident.user)
        user = ident.user

        self.assertEqual(
            log.output,
            [
                "INFO:debusine.server.signon:"
                f"{user}: auto created from identity {ident}",
                "INFO:debusine.server.signon:"
                f"{user}: auto bound to identity {ident}",
            ],
        )

        # FIXME: use assertQuerySetEqual from Django 4.2
        self.assertQuerysetEqual(user.identities.all(), [ident])

        self.assertEqual(user.username, "test@example.org")
        self.assertEqual(user.email, "test@example.org")
        self.assertEqual(user.first_name, "Test")
        self.assertEqual(user.last_name, "User")

    @override_settings(SIGNON_AUTO_CREATE_USER=True)
    def test_auto_create_user_two_idents(self):
        """Auto create and bind Django user for two new identities."""
        ident1 = Identity.objects.create(
            issuer="debsso",
            subject="user1@debian.org",
            claims={
                "name": "Test User",
                "email": "test1@example.org",
                "email_verified": True,
            },
        )
        ident2 = Identity.objects.create(
            issuer="salsa",
            subject="user2@debian.org",
            claims={
                "name": "Test User",
                "email": "test2@example.org",
                "email_verified": True,
            },
        )

        with self.assertLogs("debusine.server.signon") as log:
            self._signon(ident1, ident2)

        # Both identities have been mapped to the same user
        ident1.refresh_from_db()
        self.assertIsNotNone(ident1.user)
        user = ident1.user
        ident2.refresh_from_db()
        self.assertIsNotNone(ident2.user)
        self.assertEqual(ident2.user, user)

        # The user created matches the first identity in the provider list
        self.assertEqual(
            log.output,
            [
                "INFO:debusine.server.signon:"
                f"{user}: auto created from identity {ident1}",
                "INFO:debusine.server.signon:"
                f"{user}: auto bound to identity {ident1}",
                "INFO:debusine.server.signon:"
                f"{user}: auto bound to identity {ident2}",
            ],
        )

        # FIXME: use assertQuerySetEqual from Django 4.2
        self.assertQuerysetEqual(
            user.identities.all(), [ident1, ident2], ordered=False
        )

        self.assertEqual(user.username, "test1@example.org")
        self.assertEqual(user.email, "test1@example.org")
        self.assertEqual(user.first_name, "Test")
        self.assertEqual(user.last_name, "User")

    @override_settings(SIGNON_AUTO_CREATE_USER=True)
    def test_auto_create_user_fails(self):
        """Do not autocreate users with unverified emails."""
        ident = Identity.objects.create(
            issuer="salsa",
            subject="user@debian.org",
            claims={
                "name": "Test User",
                "email": "test@example.org",
                "email_verified": False,
            },
        )

        with self.assertLogs("debusine.server.signon") as log:
            self._signon(ident)

        ident.refresh_from_db()
        self.assertFalse(self.request.user.is_authenticated)
        self.assertIsNone(ident.user)

        self.assertEqual(
            log.output,
            [
                "WARNING:debusine.server.signon:"
                f"identity {ident} does not have a verified email"
            ],
        )

    @override_settings(
        SIGNON_AUTO_CREATE_USER=True,
        SIGNON_PROVIDERS=[
            providers.Provider(name="debsso", label="sso.debian.org"),
            providers.Provider(name="salsa", label="Salsa"),
            providers.Provider(name="gitlab", label="Gitlab"),
        ],
    )
    def test_auto_create_user_on_identity_conflict(self):
        """Do not autocreate in case of identity conflict."""
        # Recreate Signon to get the updated providers list
        self.request.signon = Signon(self.request)

        # Log in with one identity
        user1 = get_user_model().objects.create_user(
            "test1", email="test1@example.org"
        )
        ident1 = Identity.objects.create(
            user=user1,
            issuer="debsso",
            subject="test1@debian.org",
            claims={"email": "1@ex.org", "email_verified": True, "name": "Foo"},
        )

        # A new conflicting identity is activated
        user2 = get_user_model().objects.create_user(
            "test2", email="test2@example.org"
        )
        ident2 = Identity.objects.create(
            user=user2,
            issuer="salsa",
            subject="test2@debian.org",
            claims={"email": "2@ex.org", "email_verified": True, "name": "Bar"},
        )

        # Another identity, unbound
        ident3 = Identity.objects.create(
            issuer="gitlab",
            subject="test3@debian.org",
            claims={"email": "3@ex.org", "email_verified": True, "name": "Baz"},
        )

        with self.assertLogs("debusine.server.signon") as log:
            self._signon(ident1, ident2, ident3)

        self.assertEqual(
            log.output,
            [
                "ERROR:debusine.server.signon:"
                "Conflicting user mapping:"
                f" identities ({ident1}, {ident2}, {ident3})"
                f" map to at least {user1} and {user2}"
            ],
        )

        # The previous user is replaced
        self.assertFalse(self.request.user.is_authenticated)

        # Both identities are still active
        self.assertEqual(
            self.request.signon.identities,
            {
                "debsso": ident1,
                "salsa": ident2,
                "gitlab": ident3,
            },
        )

        ident1.refresh_from_db()
        self.assertEqual(ident1.user, user1)
        ident2.refresh_from_db()
        self.assertEqual(ident2.user, user2)
        ident3.refresh_from_db()
        self.assertIsNone(ident3.user)
