# Copyright 2021-2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""URLs for the server application - API."""

from django.urls import path

from debusine.server.views import (
    ArtifactRelationsView,
    ArtifactView,
    GetNextWorkRequestView,
    RegisterView,
    UpdateWorkRequestAsCompletedView,
    UpdateWorkerDynamicMetadataView,
    UploadFileView,
    WorkRequestView,
)

app_name = 'server'


urlpatterns = [
    path(
        '1.0/work-request/get-next-for-worker/',
        GetNextWorkRequestView.as_view(),
        name='work-request-get-next',
    ),
    path(
        '1.0/work-request/',
        WorkRequestView.as_view(),
        name='work-request-create',
    ),
    path(
        '1.0/work-request/<int:work_request_id>/',
        WorkRequestView.as_view(),
        name='work-request-detail',
    ),
    path(
        '1.0/work-request/<int:work_request_id>/completed/',
        UpdateWorkRequestAsCompletedView.as_view(),
        name='work-request-completed',
    ),
    path(
        "1.0/artifact/",
        ArtifactView.as_view(),
        name="artifact-create",
    ),
    path(
        "1.0/artifact/<int:artifact_id>/",
        ArtifactView.as_view(),
        name="artifact",
    ),
    path(
        "1.0/artifact/<int:artifact_id>/files/<path:file_path>/",
        UploadFileView.as_view(),
        name="upload-file",
    ),
    path('1.0/worker/register/', RegisterView.as_view(), name='register'),
    path(
        '1.0/worker/dynamic-metadata/',
        UpdateWorkerDynamicMetadataView.as_view(),
        name='worker-dynamic-metadata',
    ),
    path(
        "1.0/artifact-relation/",
        ArtifactRelationsView.as_view(),
        name="artifact-relation-list",
    ),
    path(
        "1.0/artifact-relation/<int:pk>/",
        ArtifactRelationsView.as_view(),
        name="artifact-relation-detail",
    ),
]
