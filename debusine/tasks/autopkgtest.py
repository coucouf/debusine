# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Task to use autopkgtest in debusine."""
import re
from pathlib import Path
from typing import Optional

from debusine import utils
from debusine.artifacts.local_artifact import AutopkgtestArtifact
from debusine.tasks import Task
from debusine.tasks._task_mixins import (
    FetchExecUploadMixin,
    TaskRunCommandMixin,
)

# Use TypeAlias when not supporting Python 3.9 from bullseye
ParsedSummaryFile = dict[str, dict[str, str]]


class Autopkgtest(FetchExecUploadMixin, TaskRunCommandMixin, Task):
    """Task to use autopkgtest in debusine."""

    DEFAULT_BACKEND = "podman"  # TODO: move to settings?

    TASK_VERSION = "1.0"
    TASK_DATA_SCHEMA = {
        "type": "object",
        "properties": {
            "input": {
                "type": "object",
                "properties": {
                    "source_artifact_id": {
                        "type": "integer",
                    },
                    "binary_artifacts_ids": {
                        "type": "array",
                        "items": {"type": "integer"},
                    },
                    "context_artifacts_ids": {
                        "type": "array",
                        "items": {"type": "integer"},
                    },
                },
                "required": [
                    "source_artifact_id",
                    "binary_artifacts_ids",
                ],
                "additionalProperties": False,
            },
            "architecture": {"type": "string"},
            "distribution": {"type": "string"},
            "backend": {
                "type": "string",
                "enum": ["auto", "schroot", "lxc", "qemu", "podman"],
            },
            "include_tests": {
                "type": "array",
                "items": {"type": "string"},
            },
            "exclude_tests": {
                "type": "array",
                "items": {"type": "string"},
            },
            "debug_level": {
                "type": "integer",
                "minimum": 0,
                "maximum": 3,
            },
            "extra_apt_sources": {
                "type": "array",
                "items": {"type": "string"},
            },
            "use_packages_from_base_repository": {
                "type": "boolean",
            },
            "environment": {
                "type": "object",
                "additionalProperties": {
                    "type": "string",
                },
            },
            "needs_internet": {
                "type": "string",
                "enum": ["run", "try", "skip"],
            },
            "fail_on": {
                "type": "object",
                "properties": {
                    "failed_test": {
                        "type": "boolean",
                    },
                    "flaky_test": {
                        "type": "boolean",
                    },
                    "skipped_test": {
                        "type": "boolean",
                    },
                },
                "additionalProperties": False,
            },
            "timeout": {
                "type": "object",
                "properties": {
                    "global": {
                        "type": "integer",
                        "minimum": 0,
                    },
                    "factor": {
                        "type": "integer",
                        "minimum": 0,
                    },
                    "short": {
                        "type": "integer",
                        "minimum": 0,
                    },
                    "install": {
                        "type": "integer",
                        "minimum": 0,
                    },
                    "test": {
                        "type": "integer",
                        "minimum": 0,
                    },
                    "copy": {
                        "type": "integer",
                        "minimum": 0,
                    },
                },
                "additionalProperties": False,
            },
        },
        "required": [
            "input",
            "architecture",
            "distribution",
        ],
        "additionalProperties": False,
    }

    ARTIFACT_DIR = "artifact-dir"
    SUMMARY_FILE = "artifact-dir/summary"

    def __init__(self):
        """Initialize object."""
        super().__init__()

        self._source_package_information: dict[str, str] = {}
        self._source_package_path: Optional[str] = None

        self._parsed: Optional[ParsedSummaryFile] = None
        self._autopkgtest_targets: list[Path] = []

        # self._workspace is used when uploading artifacts.
        # Currently it's set by fetch_input(), but it should be set
        # by the Worker (and this be moved to Task)
        # See #186
        self._workspace: Optional[str] = None

    def configure(self, task_data):
        """Handle autopkgtest-specific configuration."""
        super().configure(task_data)

        # Handle default values
        self.data.setdefault("backend", "auto")
        self.data.setdefault("debug_level", 0)
        self.data.setdefault("use_packages_from_base_repository", False)
        self.data.setdefault("needs_internet", "run")

        # Default options
        fail_on_defaults = {
            "failed_test": True,
            "flaky_test": False,
            "skipped_test": False,
        }

        # If self.data["fail_on"] does not exist, set the default options
        fail_on = self.data.setdefault("fail_on", fail_on_defaults)

        # self.data["fail_on"] might have been set by the user but only
        # for some values. Set the defaults
        for key, value in fail_on_defaults.items():
            fail_on.setdefault(key, value)

    def check_directory_for_consistency_errors(
        self, build_directory: Path  # noqa: U100
    ) -> list[str]:
        """Autopkgtest ARTIFACT_DIR/summary file does not exist."""
        summary_path = build_directory / self.SUMMARY_FILE
        if not summary_path.exists():
            return [f"'{self.SUMMARY_FILE}' does not exist"]

        return []

    @staticmethod
    def _parse_summary_file(summary_file: Path) -> dict[str, dict[str, str]]:
        """
        Parse autopkgtest summary file (from autopkgtest --summary).

        :param summary_file: file to parse.
        :return: dictionary with the result. Structure:
            .. code-block::
                {"test-name-1": {"status": "PASS"},
                 "test-name-2: {"status": "FAIL", "details": "partial"},
                }

            "status": always in the dictionary (PASS, FAIL, FLAKY, SKIPPED or
            any other status written by autopkgtest)
            "details": only in the dictionary if details are found
        :raises: ValueError if a line cannot be parsed
        """  # noqa: RST301
        parsed = {}
        with summary_file.open() as file:
            for line in file.readlines():
                line = line.rstrip()
                m = re.match(
                    r"(?P<name>\S+)\s+(?P<status>\S+)"
                    r"(?:\s+(?P<details>.*))?",
                    line,
                )

                if m is None:
                    raise ValueError(f"Failed to parse line: {line}")

                name = m.group("name")
                result = m.group("status")

                if result not in ("PASS", "FAIL", "SKIP", "FLAKY"):
                    raise ValueError(f"Line with unexpected result: {line}")

                parsed[name] = {"status": result}

                if details := m.group("details"):
                    parsed[name]["details"] = details

        return parsed

    def fetch_input(self, destination: Path) -> bool:
        """Download the required artifacts."""
        data_input = self.data["input"]
        artifact = self.fetch_artifact(
            data_input["source_artifact_id"], destination
        )
        self._workspace = artifact.workspace

        for file_path, file_data in artifact.files.items():
            if file_path.endswith(".dsc"):
                self._source_package_information["url"] = file_data.url
                self._source_package_path = file_path
                break

        for artifact_id in data_input["binary_artifacts_ids"]:
            self.fetch_artifact(artifact_id, destination)

        for artifact_id in data_input.get("context_artifacts_ids", []):
            self.fetch_artifact(artifact_id, destination)

        return True

    def _cmdline(self) -> list[str]:
        """
        Return autopkgtest command line (idempotent).

        Use configuration of self.data.
        """
        cmd = [
            "autopkgtest",
            "--apt-upgrade",
            f"--output-dir={self.ARTIFACT_DIR}",
            f"--summary={self.SUMMARY_FILE}",
            "--no-built-binaries",
        ]

        for include_test in self.data.get("include_tests", []):
            cmd.append(f"--test-name={include_test}")

        for exclude_test in self.data.get("exclude_tests", []):
            cmd.append(f"--skip-test={exclude_test}")

        if debug_level := self.data["debug_level"]:
            cmd.append("-" + "d" * debug_level)

        for extra_apt_source in self.data.get("extra_apt_sources", []):
            cmd.append(f"--add-apt-source={extra_apt_source}")

        if self.data["use_packages_from_base_repository"]:
            cmd.append(f"--apt-default-release={self.data['distribution']}")

        for variable, value in self.data.get("environment", {}).items():
            cmd.append(f"--env={variable}={value}")

        cmd.append(f"--needs-internet={self.data['needs_internet']}")

        for key, value in self.data.get("timeout", {}).items():
            cmd.append(f"--timeout-{key}={value}")

        cmd.extend(map(str, self._autopkgtest_targets))

        cmd.append("--")
        if (backend := self.data["backend"]) == "auto":
            cmd.append(self.DEFAULT_BACKEND)
        else:
            cmd.append(backend)

        cmd.append(self.data["distribution"])

        return cmd

    def configure_for_execution(
        self, download_directory: Path  # noqa: U100
    ) -> bool:
        """Gather information used later on (_cmdline(), upload_artifacts())."""
        # Used by upload_artifacts()
        dsc_file = download_directory / self._source_package_path
        dsc = utils.read_dsc(dsc_file)

        self._source_package_information["name"] = dsc["source"]
        self._source_package_information["version"] = dsc["version"]

        # Used by _cmdline():
        self._autopkgtest_targets = utils.find_files_suffixes(
            download_directory, [".deb"]
        )
        self._autopkgtest_targets.append(dsc_file)

        return True

    def task_succeeded(self, execute_directory: Path) -> bool:
        """
        Parse the summary file and return success.

        Use self.data["fail_on"].
        """
        self._parsed = self._parse_summary_file(
            execute_directory / self.SUMMARY_FILE
        )

        fail_on = self.data["fail_on"]

        for result in self._parsed.values():
            if (
                (result["status"] == "FAIL" and fail_on["failed_test"])
                or (result["status"] == "FLAKY" and fail_on["flaky_test"])
                or (result["status"] == "SKIP" and fail_on["skipped_test"])
            ):
                return False

        return True

    def upload_artifacts(
        self, execute_directory: Path, *, execution_success: bool  # noqa: U100
    ):
        """Upload AutopkgtestArtifact with the files, data and relationships."""
        autopkgtest_artifact = AutopkgtestArtifact.create(
            execute_directory / self.ARTIFACT_DIR
        )

        autopkgtest_artifact.data["results"] = self._parsed
        autopkgtest_artifact.data["cmdline"] = self._quote_cmd(self._cmdline())
        autopkgtest_artifact.data[
            "source_package"
        ] = self._source_package_information
        autopkgtest_artifact.data["architecture"] = self.data["architecture"]
        autopkgtest_artifact.data["distribution"] = self.data["distribution"]

        self.debusine.upload_artifact(
            autopkgtest_artifact,
            workspace=self._workspace,
            work_request=self.work_request,
        )

    def run_cmd_succeeded(self, returncode: int) -> bool:
        """Return True if autopkgtest have ran successfully."""
        return returncode not in {16, 20}
