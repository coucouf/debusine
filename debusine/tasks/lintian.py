# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Task to use lintian in debusine."""
import json
import re
import subprocess
from enum import Enum, auto
from pathlib import Path
from typing import Optional

from debian import debfile, debian_support

from debusine import utils
from debusine.artifacts import LintianArtifact
from debusine.tasks import Task
from debusine.tasks._task_mixins import (
    FetchExecUploadMixin,
    TaskRunCommandMixin,
)


class PackageType(Enum):
    """Possible package types."""

    SOURCE = auto()
    BINARY_ALL = auto()
    BINARY_ANY = auto()


class Lintian(FetchExecUploadMixin, TaskRunCommandMixin, Task):
    """Task to use lintian in debusine."""

    TASK_VERSION = "1.0"
    TASK_DATA_SCHEMA = {
        "type": "object",
        "properties": {
            "input": {
                "type": "object",
                "properties": {
                    "binary_artifacts_ids": {
                        "type": "array",
                        "items": {"type": "integer"},
                    },
                    "source_artifact_id": {
                        "type": "integer",
                    },
                },
                "anyOf": [
                    {"required": ["binary_artifacts_ids"]},
                    {"required": ["source_artifact_id"]},
                ],
            },
            "output": {
                "type": "object",
                "properties": {
                    "source_analysis": {"type": "boolean"},
                    "binary_all_analysis": {"type": "boolean"},
                    "binary_any_analysis": {"type": "boolean"},
                },
                "additionalProperties": False,
            },
            "target_distribution": {
                "type": "string",
            },
            "min_lintian_version": {
                "type": "string",
            },
            "include_tags": {
                "type": "array",
                "items": {"type": "string"},
                "description": "Passed to --tags to lintian",
            },
            "exclude_tags": {
                "type": "array",
                "items": {"type": "string"},
                "description": "Passed to --suppress-tags to lintian",
            },
            "fail_on_severity": {
                "type": "string",
                "enum": [
                    "error",
                    "warning",
                    "info",
                    "pedantic",
                    "experimental",
                    "overridden",
                    "none",
                ],
                "description": "If the analysis emits tags of this severity or "
                "higher, the task will return 'failure' instead of 'success'",
            },
        },
        "required": ["input"],
        "additionalProperties": False,
    }

    # Keep _SEVERITY_SHORT_TO_LONG ordered from higher to lower
    _SEVERITY_SHORT_TO_LONG = {
        "E": "error",
        "W": "warning",
        "I": "info",
        "P": "pedantic",
        "X": "experimental",
        "O": "overridden",
        "C": "classification",  # ignored for the fail_on_severity
        "M": "masked",  # In _IGNORED_SEVERITIES: not exposed to the user
    }

    # Severities that are not exposed to the user
    _IGNORED_SEVERITIES = {"masked"}

    CAPTURE_OUTPUT_FILENAME = "lintian.txt"

    def __init__(self):
        """Initialize object."""
        super().__init__()

        # In the lintian cmd: the target that is tested (local file, .dsc
        # or .changes)
        self._lintian_targets: Optional[list[Path]] = None

        # self._workspace is used when uploading artifacts.
        # If Lintian.input is {source,binary}_artifact_id: self._workspace
        # will be set to the workspace of the input artifact. If it's None:
        # it is not sent to the server, the server assigns it dynamically.
        self._workspace: Optional[str] = None

        # Used by _get_lintian_version() to cache the Lintian version
        self._lintian_version: Optional[str] = None

        # Package type to package names that are being analysed by
        # this Lintian invocation. In other words: packages belonging
        # to each package type.
        self._package_type_to_packages: dict[PackageType, set[str]] = {
            PackageType.SOURCE: set(),
            PackageType.BINARY_ANY: set(),
            PackageType.BINARY_ALL: set(),
        }

        self._package_name_to_filename: dict[str, str] = {}

        self._severities_to_level = {}  # dictionary with severity's name to
        # level. Higher is bigger severity
        for ranking, severity in enumerate(
            reversed(self._SEVERITY_SHORT_TO_LONG.values())
        ):
            self._severities_to_level[severity] = ranking

    def configure(self, task_data):
        """Handle lintian-specific configuration."""
        super().configure(task_data)

        # Handle default values
        self.data.setdefault("fail_on_severity", "none")
        self.data.setdefault("target_distribution", "debian:unstable")

        self.data.setdefault("output", {})
        self.data["output"].setdefault("source_analysis", True)
        self.data["output"].setdefault("binary_all_analysis", True)
        self.data["output"].setdefault("binary_any_analysis", True)

    def _create_analysis(
        self,
        parsed_output: list[dict],
        packages: set[str],
    ) -> dict[str, any]:
        """
        Parse output of lintian, return the analysis.

        :param parsed_output: output of lintian (parsed by _parse_output())
        :param packages: package names to consider for this analysis
        :return: Dictionary with "tags" key and the list of tags (sorted by
          package, severity, tag and tag_information)
        """
        # Create a copy of each tag, remove " source" from the package name
        # (analysis.json does not have " source", but "parsed_output" is
        # the output from `lintian` and has it

        # Create dictionary with all severities and 0 in the value
        tags_count_by_severity = {
            value: 0
            for value in self._SEVERITY_SHORT_TO_LONG.values()
            if value not in self._IGNORED_SEVERITIES
        }

        tags_found = set()
        overridden_tags_found = set()
        package_filename = {}
        tags_for_packages = []

        for tag in parsed_output:
            package = tag["package"]

            if package not in packages:
                continue

            package_no_source_suffix = package.removesuffix(" source")
            package_filename[
                package_no_source_suffix
            ] = self._package_name_to_filename[package]

            tags_for_packages.append(
                {**tag, "package": package_no_source_suffix}
            )

            tag_name = tag["tag"]

            tags_count_by_severity[tag["severity"]] += 1

            if tag["severity"] == "overridden":
                overridden_tags_found.add(tag_name)
            else:
                tags_found.add(tag_name)

        summary = {
            "tags_count_by_severity": tags_count_by_severity,
            "package_filename": package_filename,
            "tags_found": sorted(tags_found),
            "overridden_tags_found": sorted(overridden_tags_found),
            "lintian_version": self._get_lintian_version(),
            "distribution": self.data["target_distribution"],
        }

        return {
            "tags": tags_for_packages,
            "summary": summary,
            "version": self.TASK_VERSION,
        }

    @classmethod
    def _parse_tag_line(cls, line: str) -> Optional[dict[str, str]]:
        """
        Return dictionary with the tag information or None.

        Lintian in Debian bullseye return:
        W: hello source: package-uses-deprecated-debhelper-compat-version 9
        N:
        W: package-uses-deprecated-debhelper-compat-version

        The second "W:" line is a tag_line but _parse_tag_line() return
        None: the information is already included. This line can be ignored.
        """
        severities = "".join(cls._SEVERITY_SHORT_TO_LONG)

        m = re.match(
            fr"(?P<severity>[{severities}M]):\s(?P<package>\S+"
            fr"(?: source)?):\s(?P<tag>\S+) ?(?P<note>[^[]*)?",
            line,
        )
        # severity, package and tags are always there. Information might
        # or might not be there in line. If it's there is does NOT start
        # with [ (if it starts with [ it's a file, dealt below)

        if m is None:
            m = re.match(fr"(?P<severity>[{severities}M]):\s(?P<tag>\S+)", line)
            if m:
                # Bullseye's lintian have a duplicated tag line after each
                # real (full information). Ignore it.
                # Example:
                #
                # W: hello source: package-uses-deprecated-debhelper-compat-v 9
                # N:
                # W: package-uses-deprecated-debhelper-compat-v
                #
                # The second W: line is ignored.
                return None

            raise ValueError(f"Failed to parse line: {line}")

        parsed_tag = {
            "severity": cls._SEVERITY_SHORT_TO_LONG[m.group("severity")],
            "package": m.group("package"),
            "tag": m.group("tag"),
            "explanation": "",
            "comment": "",
            "note": (m.group("note") or "").strip(),
            "pointer": "",
        }

        m = re.match(r".*\[(?P<pointer>.*)]", line)

        if m:
            parsed_tag["pointer"] = m.group("pointer")

        return parsed_tag

    @classmethod
    def _parse_output(cls, lintian_output: Path) -> list[dict]:
        """
        Parse output of lintian, return structured representation.

        :param lintian_output: file with lintian's output
        :return: list with a dictionary per each lintian line. Keys:
            severity, package, tag, details (if it's in the output), file
            (if it's in the output)
        """
        tags = []

        tag: Optional[dict] = None
        previous_line: Optional[str] = None

        in_comment = False
        explanation: str = ""
        comment: str = ""

        with lintian_output.open() as output:
            for line in output:
                line = line.strip()
                if line == "N:" and previous_line == "N:":
                    # Comments (written by the maintainer in the lintian's
                    # override files) and explanations (all start by N:).
                    # The comments starts when there are "N:\nN:\n" in the
                    # output.
                    in_comment = True

                elif line.startswith("N:"):
                    parsed_line = line.lstrip("N:").lstrip() + "\n"
                    if in_comment:
                        comment += parsed_line
                    elif explanation != "" or parsed_line != "\n":
                        explanation += parsed_line

                else:
                    # Line not starting with "N:". It is a new tag.

                    # Add the explanation into the current tag before
                    # creating the new one
                    if tag:
                        tag["explanation"] = explanation.strip()
                        explanation = ""

                    tag = cls._parse_tag_line(line)

                    if tag is None:
                        continue

                    # If a comment was read (above the tag) add it
                    tag["comment"] = comment.strip()
                    comment = ""
                    # If we were in a comment: not anymore
                    in_comment = False

                    # Add the tag in the list, unless it is needed
                    # to be ignored (e.g. "masked")
                    if tag["severity"] not in cls._IGNORED_SEVERITIES:
                        tags.append(tag)

                previous_line = line

            # Add explanation for the last tag (usually "explanation"
            # is added when finding a new tag)
            if tag is not None:
                tag["explanation"] = explanation.strip()

        return tags

    def _cmdline(self) -> list[str]:
        """
        Build the lintian command line.

        Use configuration of self.data and self._lintian_targets.
        """
        if self.data["target_distribution"] == "jessie":
            display_level = "pedantic"
        else:
            display_level = "classification"

        cmd = [
            "lintian",
            "--no-cfg",
            "--display-level",
            f">={display_level}",
            "--display-experimental",
            "--info",
            "--show-overrides",
        ]

        if include_tags := self.data.get("include_tags"):
            include_tags = ",".join(include_tags)
            cmd.append(f"--tags={include_tags}")

        if exclude_tags := self.data.get("exclude_tags"):
            exclude_tags = ",".join(exclude_tags)
            cmd.append(f"--suppress-tags={exclude_tags}")

        cmd.extend([str(target) for target in self._lintian_targets])

        return cmd

    @staticmethod
    def _extract_package_name_type(
        file: Path,
    ) -> tuple[Optional[str], Optional[PackageType]]:
        """
        Return package name for file.

        :param file: if file ends in .deb or .udeb: use debfile.DebFile
            to return control["Package"]. If it's a .dsc: return
            pkg["Source"] + source. For other file extensions: return None.
        :return: package filename.
        """
        if file.suffix in (".deb", ".udeb"):
            pkg = debfile.DebFile(file)
            control = pkg.control.debcontrol()
            package_name = control["Package"]

            if control["Architecture"] == "all":
                package_type = PackageType.BINARY_ALL
            else:
                package_type = PackageType.BINARY_ANY

        elif file.suffix in ".dsc":
            pkg = utils.read_dsc(file)
            # Lintian identifies the binary package Vs. source package with
            # the string " source". This is removed when presented to the user
            package_name = pkg["Source"] + " source"
            package_type = PackageType.SOURCE
        else:
            package_name = None
            package_type = None

        return package_name, package_type

    def fetch_input(self, destination: Path) -> bool:
        """Download the required artifacts."""
        data_input = self.data["input"]

        if (artifact_id := data_input.get("source_artifact_id")) is not None:
            artifact = self.fetch_artifact(artifact_id, destination)
            self._workspace = artifact.workspace  # remove when #186

        for artifact_id in data_input.get("binary_artifacts_ids", []):
            artifact = self.fetch_artifact(artifact_id, destination)
            self._workspace = artifact.workspace  # remove when #186

        return True

    def can_run_on(self, worker_metadata: dict) -> bool:  # noqa: U100
        """Check the specified worker can run the requested task."""
        # TODO: which workers can run it? Check chroots with schroot --list?
        # Last discussed (IIRC): use the sbuild chroots for
        # testing/unstable/stable?
        return True

    def configure_for_execution(self, download_directory: Path) -> bool:
        """
        Find a .dsc, .deb and .udeb files in download_directory.

        Set self._lintian_targets to the relevant files.

        Verify that Lintian version is the minimal version.

        :param download_directory: where to search the files
        :return: True if valid files were found
        """
        if min_lintian_version := self.data.get("min_lintian_version"):
            worker_lintian_version = self._get_lintian_version()
            if (
                debian_support.version_compare(
                    worker_lintian_version, min_lintian_version
                )
                < 0
            ):
                self.append_to_log_file(
                    "configure_for_execution.log",
                    [
                        f"Requested minimal lintian version: "
                        f"{min_lintian_version} Worker lintian "
                        f"version: {worker_lintian_version}"
                    ],
                )
                return False

        self._lintian_targets = utils.find_files_suffixes(
            download_directory, [".dsc"]
        ) + utils.find_files_suffixes(download_directory, [".deb", ".udeb"])

        # Prepare mapping from filename to package name
        for file in download_directory.iterdir():
            package_name, arch = self._extract_package_name_type(
                download_directory / file
            )
            if package_name is None:
                continue

            self._package_name_to_filename[package_name] = file.name
            self._package_type_to_packages[arch].add(package_name)

        if len(self._lintian_targets) == 0:
            ignored_file_names = [
                file.name for file in download_directory.iterdir()
            ]
            ignored_file_names.sort()

            self.append_to_log_file(
                "configure_for_execution.log",
                [
                    f"No *.dsc, *.deb or *.udeb to be analyzed. "
                    f"Files: {ignored_file_names}"
                ],
            )
            return False
        else:
            return True

    def execution_consistency_errors(
        self, build_directory: Path  # noqa: U100
    ) -> list[str]:
        """Return list of errors."""
        files_in_directory: set[Path] = set(build_directory.iterdir())

        if (
            build_directory / self.CAPTURE_OUTPUT_FILENAME
            not in files_in_directory
        ):
            return [f"{self.CAPTURE_OUTPUT_FILENAME} not in {build_directory}"]

        return []

    def upload_artifacts(
        self, exec_directory: Path, *, execution_success: bool  # noqa: U100
    ):
        """Upload the LintianArtifact with the files and relationships."""
        lintian_file = exec_directory / self.CAPTURE_OUTPUT_FILENAME

        package_type_to_analysis = self._create_package_type_to_analysis(
            lintian_file
        )

        for artifact_type, analysis in package_type_to_analysis.items():
            (
                analysis_file := exec_directory
                / f"analysis-{artifact_type}.json"
            ).write_text(json.dumps(analysis))

            lintian_artifact = LintianArtifact.create(
                analysis=analysis_file, lintian_output=lintian_file
            )

            lintian_artifact.data["summary"] = analysis["summary"]

            uploaded = self.debusine.upload_artifact(
                lintian_artifact,
                workspace=self._workspace,
                work_request=self.work_request,
            )

            for source_artifact_id in self._source_artifacts_ids:
                self.debusine.relation_create(
                    uploaded.id, source_artifact_id, "relates-to"
                )

    def _create_package_type_to_analysis(
        self, lintian_file: Path
    ) -> dict[PackageType, dict[str, any]]:
        """
        Read lintian_file, return dict with PackageType to analysis.

        :param lintian_file: Lintian output file
        :return: dict[PackageType, Analysis]: only for the PackageTypes in
          self.data["output"]
        """
        parsed_output = self._parse_output(lintian_file)

        parsed_output.sort(
            key=lambda x: (
                x["package"],
                -self._severities_to_level[
                    x["severity"]
                ],  # negative to sort from highest to lowest
                x["tag"],
                x["note"],
            )
        )

        package_analysis_mapping = {
            PackageType.SOURCE: "source_analysis",
            PackageType.BINARY_ALL: "binary_all_analysis",
            PackageType.BINARY_ANY: "binary_any_analysis",
        }
        output = self.data["output"]

        package_type_to_analysis: dict[PackageType, dict[str, any]] = {}

        for package_type in PackageType:
            if not output[package_analysis_mapping[package_type]]:
                # self.data["output"]["source_analysis"] (or
                # "binary_all_analysis", or "binary_any_analysis") was disabled
                continue

            package_type_to_analysis[package_type] = self._create_analysis(
                parsed_output, self._package_type_to_packages[package_type]
            )

        return package_type_to_analysis

    def task_succeeded(self, execute_directory: Path) -> bool:
        """
        Evaluate task output and return success.

        For a successful run of lintian:
        -lintian must have generated the output file
        -no tags of severity self.data["fail_on"] or higher

        :return: True for success, False failure.
        """
        fail_on_severity = self.data["fail_on_severity"]

        if fail_on_severity == "none":
            return True

        fail_on_severity_level = self._severities_to_level[fail_on_severity]
        lintian_file = execute_directory / self.CAPTURE_OUTPUT_FILENAME
        parsed_output = self._parse_output(lintian_file)

        for tag in parsed_output:
            if (
                self._severities_to_level[tag["severity"]]
                >= fail_on_severity_level
            ):
                return False

        return True

    def _get_lintian_version(self) -> str:
        if self._lintian_version is not None:
            return self._lintian_version

        out = subprocess.check_output(["lintian", "--version"])

        m = re.match("Lintian v(.*)", out.decode("utf-8", errors="ignore"))

        self._lintian_version = m.group(1)
        return self._lintian_version
