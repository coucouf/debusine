# Copyright 2023 The Debusine developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Unit tests for the Autopkgtest class."""
import copy
import datetime
import textwrap
from pathlib import Path
from unittest import TestCase, mock
from unittest.mock import MagicMock, call

from debusine.artifacts.local_artifact import AutopkgtestArtifact
from debusine.client.debusine import Debusine
from debusine.client.models import ArtifactResponse
from debusine.tasks import TaskConfigError
from debusine.tasks.autopkgtest import Autopkgtest
from debusine.tasks.tests.helper_mixin import TaskHelperMixin
from debusine.test import TestHelpersMixin


class AutopkgtestTests(TestHelpersMixin, TaskHelperMixin, TestCase):
    """Tests for Autopkgtest."""

    SAMPLE_TASK_DATA = {
        "input": {
            "source_artifact_id": 5,
            "binary_artifacts_ids": [10, 11],
        },
        "architecture": "amd64",
        "distribution": "debian:bookworm",
    }

    def setUp(self):
        """Initialize test."""
        self.task = Autopkgtest()
        self.configure_task()

    def assert_parse_summary_file_expected(
        self, autopkgtest_summary: str, expected: dict
    ):
        """Call self.autopkgtest.parse_summary_file(summary), assert result."""
        summary_file = self.create_temporary_file(
            contents=autopkgtest_summary.encode("utf-8")
        )
        actual = Autopkgtest._parse_summary_file(summary_file)

        self.assertEqual(actual, expected)

    def test_parse_summary_file_empty(self):
        """Parse an empty file."""
        contents = ""
        expected = {}

        self.assert_parse_summary_file_expected(contents, expected)

    def test_parse_summary_file_two_tests(self):
        """Parse file with two tests and two different results."""
        contents = textwrap.dedent(
            """\
            unit-tests-server    PASS
            integration-tests-task-sbuild FLAKY
        """
        )
        expected = {
            "unit-tests-server": {"status": "PASS"},
            "integration-tests-task-sbuild": {"status": "FLAKY"},
        }

        self.assert_parse_summary_file_expected(contents, expected)

    def test_parse_summary_file_expected_results(self):
        """Parse file with valid results."""
        contents = textwrap.dedent(
            """\
            test-name-1    PASS
            test-name-2     FAIL
            test-name-3     SKIP
            test-name-4     FLAKY
            """
        )
        expected = {
            "test-name-1": {"status": "PASS"},
            "test-name-2": {"status": "FAIL"},
            "test-name-3": {"status": "SKIP"},
            "test-name-4": {"status": "FLAKY"},
        }
        self.assert_parse_summary_file_expected(contents, expected)

    def test_parse_summary_file_unexpected_result_raise_value_error(self):
        """Parse file with unexpected result: raise ValueError."""
        contents = "test-name-1: something"
        with self.assertRaisesRegex(
            ValueError, f"Line with unexpected result: {contents}"
        ):
            self.assert_parse_summary_file_expected(contents, {})

    def test_parse_summary_file_one_test_with_details(self):
        """Parse a file with details."""
        contents = """autodep8-python3     PASS (superficial)"""
        expected = {
            "autodep8-python3": {"status": "PASS", "details": "(superficial)"}
        }

        self.assert_parse_summary_file_expected(contents, expected)

    def test_parse_summary_file_with_details_with_space(self):
        """Parse a file with details containing a space."""
        contents = textwrap.dedent(
            """\
            unit-tests-server    PASS
            unit-tests-client  FAIL (the details)
            unit-tests-worker  SKIP stderr: Paths: foo not set, '/tmp/test'
        """
        )
        expected = {
            "unit-tests-server": {"status": "PASS"},
            "unit-tests-client": {
                "status": "FAIL",
                "details": "(the details)",
            },
            "unit-tests-worker": {
                "status": "SKIP",
                "details": "stderr: Paths: foo not set, '/tmp/test'",
            },
        }

        self.assert_parse_summary_file_expected(contents, expected)

    def test_parse_summary_file_invalid_line(self):
        """Parse a file with an invalid line."""
        # Format of each line should be "test-name test-status"
        contents = "invalid"
        with self.assertRaisesRegex(ValueError, "^Failed to parse line"):
            self.assert_parse_summary_file_expected(contents, {})

    def test_configure_valid_schema(self):
        """Valid task data do not raise any exception."""
        self.configure_task()

    def test_configure_missing_required_source_artifact_id(self):
        """Missing required field "source_artifact_id": raise exception."""
        task_no_source_artifact = copy.deepcopy(self.SAMPLE_TASK_DATA)
        del task_no_source_artifact["input"]["source_artifact_id"]

        with self.assertRaises(TaskConfigError):
            self.configure_task(task_data=task_no_source_artifact)

    def test_configure_missing_required_binary_artifacts_ids(self):
        """Missing required field "binary_artifacts_ids": raise exception."""
        task_no_binary_artifacts = copy.deepcopy(self.SAMPLE_TASK_DATA)
        del task_no_binary_artifacts["input"]["binary_artifacts_ids"]

        with self.assertRaises(TaskConfigError):
            self.configure_task(task_data=task_no_binary_artifacts)

    def test_configure_accept_context_artifacts_ids(self):
        """Optional field "context_artifacts_ids": do not raise exception."""
        task_context_artifacts_ids = copy.deepcopy(self.SAMPLE_TASK_DATA)
        task_context_artifacts_ids["input"]["context_artifacts_ids"] = [20, 21]

        self.configure_task(task_data=task_context_artifacts_ids)

    def test_configure_input_additional_property(self):
        """Additional property in input: raise exception."""
        additional_property = copy.deepcopy(self.SAMPLE_TASK_DATA)
        additional_property["input"]["something"] = "test"

        with self.assertRaises(TaskConfigError):
            self.configure_task(task_data=additional_property)

    def test_configure_additional_property(self):
        """Additional property: raise exception."""
        with self.assertRaises(TaskConfigError):
            self.configure_task(override={"something": "test"})

    def test_configure_architecture_is_required(self):
        """Missing required field "architecture": raise exception."""
        with self.assertRaises(TaskConfigError):
            self.configure_task(remove=["architecture"])

    def test_configure_distribution_is_required(self):
        """Missing required field "distribution": raise exception."""
        with self.assertRaises(TaskConfigError):
            self.configure_task(remove=["distribution"])

    def test_configure_backend_choices(self):
        """Field "backend" accept correct values."""
        for backend in ["auto", "schroot", "lxc", "qemu", "podman"]:
            self.configure_task(override={"backend": backend})

    def test_configure_backend_invalid_choice(self):
        """Field "backend" do not accept unexpected value."""
        with self.assertRaises(TaskConfigError):
            self.configure_task(override={"backend": "invalid"})

    def test_configure_backend_default_value(self):
        """Field "backend" default value is "auto"."""
        self.configure_task(remove=["backend"])
        self.assertEqual(self.task.data["backend"], "auto")

    def test_configure_backend_include_tests(self):
        """Field "include_tests" is allowed."""
        self.configure_task(override={"include_tests": ["a", "b"]})

    def test_configure_backend_exclude_tests(self):
        """Field "exclude_tests" is allowed."""
        self.configure_task(override={"exclude_tests": ["a", "b"]})

    def test_configure_debug_level(self):
        """Field "debug_level" is allowed."""
        self.configure_task(override={"debug_level": 3})

    def test_configure_debug_level_too_small_too_big(self):
        """Field "debug_level" must be between 0 and 3."""
        with self.assertRaises(TaskConfigError):
            self.configure_task(override={"debug_level": -1})

        with self.assertRaises(TaskConfigError):
            self.configure_task(override={"debug_level": 4})

    def test_configure_debug_level_default_to_0(self):
        """Field "debug_level" default value is 0."""
        self.configure_task()
        self.assertEqual(self.task.data["debug_level"], 0)

    def test_configure_extra_apt_sources(self):
        """Field "extra_apt_sources" is allowed."""
        self.configure_task(override={"extra_apt_sources": ["A", "B"]})

    def test_configure_use_packages_from_base_repository(self):
        """Field "use_package_from_base_repository" is allowed."""
        self.configure_task(
            override={"use_packages_from_base_repository": True}
        )

    def test_configure_use_packages_from_base_repository_default_is_false(self):
        """Field "use_package_from_base_repository" default value is False."""
        self.configure_task()
        self.assertFalse(self.task.data["use_packages_from_base_repository"])

    def test_configure_needs_internet_accepted_values(self):
        """Field "needs_internet" accept valid values."""
        for needs_internet in ["run", "try", "skip"]:
            self.configure_task(override={"needs_internet": needs_internet})

    def test_configure_needs_internet_reject_unexpected_value(self):
        """Field "needs_internet" do not accept unexpected value."""
        with self.assertRaises(TaskConfigError):
            self.configure_task(override={"needs_internet": "yes"})

    def test_configure_needs_internet_default_value(self):
        """Field "needs_internet" default value is "run"."""
        self.configure_task()
        self.assertEqual(self.task.data["needs_internet"], "run")

    def test_configure_environment(self):
        """Field "environment" is accepted."""
        self.configure_task(
            override={"environment": {"var1": "foo1", "var2": "foo2"}}
        )

    def test_configure_fail_on_defaults(self):
        """Field "fail_on" default values are the expected ones."""
        self.configure_task()
        self.assertEqual(
            self.task.data["fail_on"],
            {"failed_test": True, "flaky_test": False, "skipped_test": False},
        )

    def test_configure_fail_on_failed_skipped_test_defaults(self):
        """Field "fail_on" use defaults when specifying only "flaky_test"."""
        self.configure_task(override={"fail_on": {"flaky_test": False}})

        self.assertEqual(
            self.task.data["fail_on"],
            {"failed_test": True, "flaky_test": False, "skipped_test": False},
        )

    def test_configure_flaky_skipped_test_defaults(self):
        """Field "fail_on" use defaults when specifying only "failed_test"."""
        self.configure_task(override={"fail_on": {"failed_test": False}})

        self.assertEqual(
            self.task.data["fail_on"],
            {"failed_test": False, "flaky_test": False, "skipped_test": False},
        )

    def test_configure_fail_on_unexpected(self):
        """Field "fail_on" with invalid key: raise TaskConfigError."""
        with self.assertRaises(TaskConfigError):
            self.configure_task(override={"fail_on": {"non-valid": False}})

    def test_configure_timeout_valid_values(self):
        """Field "timeout" accept the expected keys."""
        for timeout_key in [
            "global",
            "factor",
            "short",
            "install",
            "test",
            "copy",
        ]:
            self.configure_task(override={"timeout": {timeout_key: 600}})

    def test_configure_timeout_non_valid_values(self):
        """Field "timeout" invalid values must be > 0: raise TaskConfigError."""
        for timeout_key, value in [
            ("global", -1),
            ("factor", -10),
            ("short", -10),
            ("install", -20),
            ("test", -30),
            ("copy", -10),
        ]:
            with self.assertRaises(TaskConfigError):
                self.configure_task(override={"timeout": {timeout_key: value}})

    def test_configure_timeout_non_valid_key(self):
        """Field "timeout" invalid key: raise TaskConfigError."""
        with self.assertRaises(TaskConfigError):
            self.configure_task(override={"timeout": {"non-valid-key": 600}})

    def test_configure_timeout_invalid_value(self):
        """Field "timeout" do not accept unexpected value."""
        with self.assertRaises(TaskConfigError):
            self.configure_task(override={"timeout": "something"})

    def test_cmdline_minimum_options(self):
        """Command line has the minimum options."""
        minimum_options = [
            "autopkgtest",
            "--apt-upgrade",
            f"--output-dir={Autopkgtest.ARTIFACT_DIR}",
            f"--summary={Autopkgtest.SUMMARY_FILE}",
            "--no-built-binaries",
        ]
        cmdline = self.task._cmdline()
        self.assertEqual(cmdline[: len(minimum_options)], minimum_options)

    def test_cmdline_include_tests(self):
        """Command line include --test-name=XXX."""
        include_tests = ["test1", "test2"]

        self.task.data["include_tests"] = include_tests

        cmdline = self.task._cmdline()

        for include_test in include_tests:
            self.assertIn(f"--test-name={include_test}", cmdline)

    def test_cmdline_exclude_tests(self):
        """Command line include --skip-test=XXX."""
        exclude_tests = ["test1", "test2"]

        self.task.data["exclude_tests"] = exclude_tests

        cmdline = self.task._cmdline()

        for exclude_test in exclude_tests:
            self.assertIn(f"--skip-test={exclude_test}", cmdline)

    def test_cmdline_debug_level(self):
        """Command line include "-dd" (debug_level)."""
        self.task.data["debug_level"] = 2

        cmdline = self.task._cmdline()

        self.assertIn("-dd", cmdline)

    def test_cmdline_extra_apt_sources(self):
        """Command line include "--add-apt-source=XXX"."""
        extra_apt_sources = [
            "deb https://deb.debian.org/debian bookworm main non-free "
            "non-free-firmware contrib",
            "deb https://deb.debian.org/debian-security bookworm-security "
            "main non-free contrib",
        ]
        self.task.data["extra_apt_sources"] = extra_apt_sources

        cmdline = self.task._cmdline()

        for extra_apt_source in extra_apt_sources:
            self.assertIn(f"--add-apt-source={extra_apt_source}", cmdline)

    def test_cmdline_apt_default_release_not_included(self):
        """Command line NOT include "--apt-default-release={distribution}."""
        self.task.data["use_packages_from_base_repository"] = False
        cmdline = self.task._cmdline()
        self.assertNotIn(
            f"--apt-default-release={self.task.data['distribution']}", cmdline
        )

    def test_cmdline_apt_default_release_included(self):
        """Command line include --"apt-default-release=distribution"."""
        self.task.data["use_packages_from_base_repository"] = True
        cmdline = self.task._cmdline()
        self.assertIn(
            f"--apt-default-release={self.task.data['distribution']}", cmdline
        )

    def test_cmdline_environment(self):
        """Command line include "--env=VAR=VALUE" for environment variables."""
        environment = {"DEBUG": "1", "LOG": "NONE"}
        self.task.data["environment"] = environment

        cmdline = self.task._cmdline()

        for var, value in environment.items():
            self.assertIn(f"--env={var}={value}", cmdline)

    def test_cmdline_add_backend_distribution(self):
        """Command line include "-- backend"."""
        backend = "lxc"
        distribution = "debian:bookworm"

        self.task.data["backend"] = backend
        self.task.data["distribution"] = distribution

        cmdline = self.task._cmdline()

        podman_index = cmdline.index(backend)
        self.assertGreater(podman_index, 0)
        self.assertEqual(cmdline[podman_index - 1], "--")
        self.assertEqual(cmdline[podman_index + 1], distribution)

    def test_cmdline_add_default_backend(self):
        """Command line include "-- Autopkgtest.DEFAULT_BACKEND"."""
        self.task.data["backend"] = "auto"

        cmdline = self.task._cmdline()

        self.assertIn(Autopkgtest.DEFAULT_BACKEND, cmdline)

    def test_cmdline_add_targets(self):
        """Targets for autopkgtest are added."""
        binary_path = Path("/tmp/download-dir/package.deb")
        source_path = Path("/tmp/download-dir/package.dsc")

        self.task._autopkgtest_targets = [binary_path, source_path]
        cmdline = self.task._cmdline()

        binary_path_index = cmdline.index(str(binary_path))

        self.assertGreater(binary_path_index, 0)
        self.assertEqual(cmdline[binary_path_index + 1], str(source_path))

    def test_needs_internet(self):
        """Command line include "--needs-internet"."""
        needs_internet = "try"

        self.task.data["needs_internet"] = needs_internet

        cmdline = self.task._cmdline()

        self.assertIn(f"--needs-internet={needs_internet}", cmdline)

    def test_timeout(self):
        """Command line include "--timeout-KEY=VALUE."."""
        timeout = {"global": 60, "factor": 600}

        self.task.data["timeout"] = timeout

        cmdline = self.task._cmdline()

        for key, value in timeout.items():
            self.assertIn(f"--timeout-{key}={value}", cmdline)

    def mock_debusine(self) -> MagicMock:
        """Create a Debusine mock and configure self.task for it. Return it."""
        # TODO: refactor from Lintian.mock_debusine() and move to
        # TasksHelpersMixin
        debusine_mock = mock.create_autospec(spec=Debusine)

        # Worker would set the server
        self.task.configure_server_access(debusine_mock)

        return debusine_mock

    def test_fetch_input_no_context_artifacts_ids(self):
        """Download "source_artifact_id" and "binary_artifacts_ids"."""
        debusine_mock = self.mock_debusine()
        workspace = "Testing"
        debusine_mock.download_artifact.return_value = ArtifactResponse(
            id=10,
            workspace=workspace,
            category="Test",
            created_at=datetime.datetime.now(),
            data={},
            download_tar_gz_url="https://example.com/not-used",
            files={},
            files_to_upload=[],
        )

        workspace = "Testing"
        source_package_url = "http://example.com/artifact/15/hello.dsc"

        files = {
            "hello.orig.tar.gz": {
                "size": 4000,
                "checksums": {},
                "type": "file",
                "url": "http://example.com/artifact/1/hello.orig.tar.gz",
            },
            "hello.dsc": {
                "size": 258,
                "checksums": {},
                "type": "file",
                "url": source_package_url,
            },
        }

        debusine_mock.download_artifact.return_value = ArtifactResponse(
            id=10,
            workspace=workspace,
            category="Test",
            created_at=datetime.datetime.now(),
            data={},
            download_tar_gz_url="https://example.com/not-used",
            files=files,
            files_to_upload=[],
        )

        destination = self.create_temporary_directory()

        self.task.fetch_input(destination)

        data_input = self.task.data["input"]

        calls = [
            call(data_input["source_artifact_id"], destination, tarball=False)
        ]

        for binary_artifact_id in data_input["binary_artifacts_ids"]:
            calls.append(call(binary_artifact_id, destination, tarball=False))

        debusine_mock.download_artifact.assert_has_calls(calls)

        self.assertEqual(self.task._workspace, workspace)
        self.assertEqual(
            self.task._source_package_information["url"], source_package_url
        )
        self.assertEqual(self.task._source_package_path, "hello.dsc")

    def test_fetch_input_context_artifacts_ids(self):
        """Download the "context_artifacts_ids"."""
        debusine_mock = self.mock_debusine()

        destination = self.create_temporary_directory()

        data_input = self.task.data["input"]

        data_input["context_artifacts_ids"] = [50, 51]

        self.task.fetch_input(destination)

        calls = []
        for context_artifact_id in data_input["context_artifacts_ids"]:
            calls.append(call(context_artifact_id, destination, tarball=False))

        debusine_mock.download_artifact.assert_has_calls(calls)

    def test_configure_for_execution(self):
        """Autopkgtest configure_for_execution() return True."""
        directory = self.create_temporary_directory()

        self.task._source_package_path = "hello.dsc"

        dsc = self.write_dsc_example_file(
            directory / self.task._source_package_path
        )

        self.assertTrue(self.task.configure_for_execution(directory))

        self.assertEqual(
            self.task._source_package_information,
            {
                "name": dsc["Source"],
                "version": dsc["Version"],
            },
        )

    def setup_task_succeeded(
        self, test_result: str, fail_on_config: dict[str, bool]
    ) -> Path:
        """
        Write autopkgtest summary file with one test output, configure task.

        :param test_result: autopkgtest test result (FAIL, FLAKY...)
        :param fail_on_config: configure task with fail_on = fail_on_config.
        :return: directory with the summary file.
        """
        directory = self.create_temporary_directory()
        (directory / Autopkgtest.ARTIFACT_DIR).mkdir()
        summary_file = directory / Autopkgtest.SUMMARY_FILE
        summary_file.write_text(f"test_name\t{test_result}")

        self.configure_task(override={"fail_on": fail_on_config})

        return directory

    def test_task_succeeded_failed_test_fail_on_failed(self):
        """Test failed, fail on failed_test=True. Failure."""
        directory = self.setup_task_succeeded("FAIL", {"failed_test": True})
        self.assertFalse(self.task.task_succeeded(directory))

    def test_task_succeeded_failed_test_do_not_fail_on_failed(self):
        """Test failed, fail on failed_test=False. Success."""
        directory = self.setup_task_succeeded("FAIL", {"failed_test": False})
        self.assertTrue(self.task.task_succeeded(directory))

    def test_task_succeed_flaky_test_fail_on_flaky_failure(self):
        """Test is flaky, fail on flaky_test=True. Failure."""
        directory = self.setup_task_succeeded("FLAKY", {"flaky_test": True})
        self.assertFalse(self.task.task_succeeded(directory))

    def test_task_succeed_flaky_test_do_not_fail_on_flaky_success(self):
        """Test is flaky, fail on flaky_test=False. Success."""
        directory = self.setup_task_succeeded("FLAKY", {"flaky_test": False})
        self.assertTrue(self.task.task_succeeded(directory))

    def test_task_succeeded_skipped_test_fail_on_skipped_success(self):
        """Test is skipped, fail on skipped_test=True. Failure."""
        directory = self.setup_task_succeeded("SKIP", {"skipped_test": True})
        self.assertFalse(self.task.task_succeeded(directory))

    def test_task_succeeded_skipped_test_do_not_fail_on_skipped_failure(self):
        """Test is skipped, fail on skipped_test=False. Success."""
        directory = self.setup_task_succeeded("SKIP", {"skipped_test": False})
        self.assertTrue(self.task.task_succeeded(directory))

    def test_upload_artifacts(self):
        """Upload the AutopkgtestArtifact."""
        exec_dir = self.create_temporary_directory()

        # Debusine.upload_artifact is mocked to verify the call only
        debusine_mock = self.mock_debusine()

        # Worker would set the server
        self.task.configure_server_access(debusine_mock)

        # self.task._workspace and self.task.workspace are set by the Worker.
        workspace_name = "testing"
        self.task._workspace = workspace_name
        work_request = 170
        self.task.work_request = work_request

        results = {"testname": {"result": "PASS"}}
        self.task._parsed = results

        self.task._source_package_information = {
            "name": "hello",
            "version": "2.0.5b",
            "url": "https://some-url.com/test",
        }

        self.task.upload_artifacts(exec_dir, execution_success=True)

        calls = []

        expected_autopkgtest_artifact = AutopkgtestArtifact.create(exec_dir)
        expected_autopkgtest_artifact.data["results"] = results
        expected_autopkgtest_artifact.data["cmdline"] = self.task._quote_cmd(
            self.task._cmdline()
        )
        expected_autopkgtest_artifact.data[
            "source_package"
        ] = self.task._source_package_information
        expected_autopkgtest_artifact.data["architecture"] = self.task.data[
            "architecture"
        ]
        expected_autopkgtest_artifact.data["distribution"] = self.task.data[
            "distribution"
        ]

        calls.append(
            call(
                expected_autopkgtest_artifact,
                workspace=workspace_name,
                work_request=work_request,
            )
        )

        debusine_mock.upload_artifact.assert_has_calls(calls)

    def test_check_directory_for_consistency_missing_summary(self):
        """Return missing ARTIFACT_DIR/summary file."""
        build_directory = self.create_temporary_directory()

        self.assertEqual(
            self.task.check_directory_for_consistency_errors(build_directory),
            [f"'{self.task.SUMMARY_FILE}' does not exist"],
        )

    def test_check_directory_for_consistency_all_good(self):
        """Return no errors ARTIFACT_DIR/summary file."""
        build_directory = self.create_temporary_directory()
        (build_directory / self.task.ARTIFACT_DIR).mkdir()
        (build_directory / self.task.ARTIFACT_DIR / "summary").write_text(
            "test-results"
        )

        self.assertEqual(
            self.task.check_directory_for_consistency_errors(build_directory),
            [],
        )

    def test_run_cmd_succeeded(self):
        """run_cmd_succeeded() return value is correct."""
        self.assertTrue(self.task.run_cmd_succeeded(0))
        self.assertTrue(self.task.run_cmd_succeeded(2))
        self.assertFalse(self.task.run_cmd_succeeded(16))
        self.assertFalse(self.task.run_cmd_succeeded(20))
