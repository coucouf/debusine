# Copyright 2021-2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Unit tests for the sbuild task support on the worker client."""
import itertools
import textwrap
from pathlib import Path
from typing import Optional
from unittest import TestCase, mock
from unittest.mock import MagicMock, call

from debusine import utils
from debusine.artifacts import (
    BinaryPackages,
    BinaryUpload,
    PackageBuildLog,
)
from debusine.client.debusine import Debusine
from debusine.client.models import RemoteArtifact
from debusine.tasks import TaskConfigError
from debusine.tasks.sbuild import Sbuild
from debusine.tasks.tests.helper_mixin import TaskHelperMixin
from debusine.test import TestHelpersMixin


class SbuildTaskTests(TestHelpersMixin, TaskHelperMixin, TestCase):
    """Test the SbuildTask class."""

    SAMPLE_TASK_DATA = {
        "input": {
            "source_artifact_id": 5,
        },
        "distribution": "bullseye",
        "host_architecture": "amd64",
        "build_components": [
            "any",
            "all",
        ],
        "sbuild_options": [
            "--post-build-commands=/usr/local/bin/post-process %SBUILD_CHANGES",
        ],
    }

    def setUp(self):
        """
        Set a path to the ontology files used in the debusine tests.

        If the worker is moved to a separate source package, this will
        need to be updated.
        """
        self.task = Sbuild()

        self.task._sbuild_package = (
            "http://deb.debian.org/debian/pool/main/h/hello/hello_2.10-2.dsc"
        )

        self.task.logger.disabled = True

        self.task.configure(self.configuration("amd64", ["any"]))

        task_call_schroot_list_patcher = mock.patch.object(
            self.task, "_call_schroot_list"
        )
        self.schroot_list_mock = task_call_schroot_list_patcher.start()
        self.schroot_list_mock.return_value = "chroot:bullseye-amd64-sbuild"
        self.addCleanup(task_call_schroot_list_patcher.stop)

        task_call_dpkg_architecture_patcher = mock.patch.object(
            self.task, "_call_dpkg_architecture"
        )
        self.dpkg_architecture_mock = (
            task_call_dpkg_architecture_patcher.start()
        )
        self.dpkg_architecture_mock.return_value = "amd64"
        self.addCleanup(task_call_dpkg_architecture_patcher.stop)

    def tearDown(self):
        """Delete self.task._debug_log_files_directory."""
        if self.task._debug_log_files_directory is not None:
            self.task._debug_log_files_directory.cleanup()

    def mock_cmdline(self, cmdline: list):
        """Patch self.task to return cmdline."""
        patcher = mock.patch.object(self.task, "_cmdline")
        self.cmdline_mock = patcher.start()
        self.cmdline_mock.return_value = cmdline
        self.addCleanup(patcher.stop)

    def test_configure_with_unknown_data(self):
        """
        Configure fails if a non-recognised key is in task_data.

        Sbuild.TASK_DATA_SCHEMA has "additionalProperties": False.
        """
        with self.assertRaises(TaskConfigError):
            self.configure_task(override={"extra_field": "extra_value"})

    def test_configure_fails_with_missing_required_data(self):
        """Configure fails with missing required keys in task_data."""
        for key in ("input", "distribution", "host_architecture"):
            with self.subTest(f"Configure with key {key} missing"):
                with self.assertRaises(TaskConfigError):
                    self.configure_task(remove=[key])

    def test_configure_fails_required_input(self):
        """Configure fails: missing source_artifact_id in task_data["input"]."""
        with self.assertRaises(TaskConfigError):
            self.configure_task(override={"input": {}})

    def test_configure_fails_extra_properties_input(self):
        """Configure fails: extra properties in task_data["input"]."""
        with self.assertRaises(TaskConfigError):
            self.configure_task(
                override={
                    "input": {
                        "source_artifact_id": 5,
                        "source_artifact_url": "https://deb.debian.org/f.dsc",
                    }
                }
            )

    def test_configure_sets_default_values(self):
        """Optional task data have good default values."""
        self.configure_task(remove=["build_components", "sbuild_options"])

        self.assertEqual(self.task.data.get("build_components"), ["any"])
        self.assertEqual(self.task.data.get("sbuild_options"), [])

    def test_configure_fails_with_bad_build_components(self):
        """Configure fails with invalid build components."""
        with self.assertRaises(TaskConfigError):
            self.configure_task(override={"build_components": ["foo", "any"]})

    def test_update_chroots_list_skips_non_desired_chroots(self):
        """source:foo and non foo-sbuild chroots are ignored."""
        self.schroot_list_mock.return_value = (
            "source:jessie-amd64-sbuild\nchroot:buster-amd64\n"
            "chroot:wanted-sbuild"
        )

        self.task._update_chroots_list()
        self.task._update_chroots_list()  # second call triggers no-op branch

        self.assertEqual(self.task.chroots, ["wanted"])

    def test_fetch_input(self):
        """Test fetch_input: call fetch_artifact(artifact_id, directory)."""
        patcher = mock.patch.object(self.task, "fetch_artifact", autospec=True)
        mocked = patcher.start()
        mocked.return_value = True
        self.addCleanup(patcher.stop)

        directory = self.create_temporary_directory()

        artifact_id = 6
        self.configure_task(
            override={"input": {"source_artifact_id": artifact_id}}
        )

        self.task.fetch_input(directory)

        mocked.assert_called_once_with(artifact_id, directory)

    def test_cmdline_starts_with_sbuild_no_clean(self):
        """Test fixed command line parameters."""
        self.configure_task()

        cmdline = self.task._cmdline()

        self.assertEqual(cmdline[0], "sbuild")
        self.assertEqual(cmdline[1], "--no-clean")

    def test_cmdline_contains_arch_parameter(self):
        """Ensure --arch parameter is computed from data."""
        self.schroot_list_mock.return_value = "chroot:bullseye-mipsel-sbuild"
        self.configure_task(override={"host_architecture": "mipsel"})
        cmdline = self.task._cmdline()

        self.assertIn("--arch=mipsel", cmdline)

    def test_cmdline_contains_dist_parameter(self):
        """Ensure --dist parameter is computed from data."""
        self.schroot_list_mock.return_value = "chroot:jessie-amd64-sbuild"
        self.configure_task(override={"distribution": "jessie"})

        cmdline = self.task._cmdline()

        self.assertIn("--dist=jessie", cmdline)

    def test_cmdline_translation_of_build_components(self):
        """Test handling of build components."""
        option_mapping = {
            "any": ["--arch-any", "--no-arch-any"],
            "all": ["--arch-all", "--no-arch-all"],
            "source": ["--source", "--no-source"],
        }
        keywords = option_mapping.keys()
        for combination in itertools.chain(
            itertools.combinations(keywords, 1),
            itertools.combinations(keywords, 2),
            itertools.combinations(keywords, 3),
        ):
            with self.subTest(f"Test build_components={combination}"):
                self.configure_task(
                    override={"build_components": list(combination)}
                )
                cmdline = self.task._cmdline()
                for key, value in option_mapping.items():
                    if key in combination:
                        self.assertIn(value[0], cmdline)
                        self.assertNotIn(value[1], cmdline)
                    else:
                        self.assertNotIn(value[0], cmdline)
                        self.assertIn(value[1], cmdline)

    def test_cmdline_contains_sbuild_options(self):
        """Ensure sbuild_options are passed just before source package url."""
        self.configure_task(override={"sbuild_options": ["--foobar"]})

        cmdline = self.task._cmdline()

        self.assertEqual("--foobar", cmdline[-2])

    def test_execute_fails_with_unsupported_distribution(self):
        """execute() fails when the requested distribution is unsupported."""
        # Default mocked setup only support bullseye
        self.configure_task(override={"distribution": "jessie"})
        with self.assertRaises(TaskConfigError):
            self.task.execute()

    def test_execute_fails_with_no_available_chroots(self):
        """execute() fails when no sbuild chroots are available."""
        self.schroot_list_mock.return_value = ""
        self.configure_task()
        with self.assertRaises(TaskConfigError):
            self.task.execute()

    def patch_temporary_directory(self, directories=1) -> list[Path]:
        """
        Patch sbuild.tempfile.TemporaryDirectory to return fixed directories.

        :param directories: number of directories that will be returned.
        :return: list with the Paths that have been created and will be
          returned.
        """
        temporary_directories = []
        for _ in range(0, directories):
            temporary_directories.append(self.create_temporary_directory())

        class MockedPathContextManager:
            def __init__(self, directory_paths: list[Path]):
                self.paths = iter(directory_paths)

            def __enter__(self):
                return next(self.paths)

            def __exit__(self, exc_type, exc_val, exc_tb):
                pass

        patcher = mock.patch.object(
            self.task, "_temporary_directory", autospec=True
        )
        temporary_directory_mock = patcher.start()
        temporary_directory_mock.return_value = MockedPathContextManager(
            temporary_directories
        )
        self.addCleanup(patcher.stop)

        return temporary_directories

    def run_execute(self, cmdline: list[str]) -> bool:
        """Run Sbuild.execute() mocking cmdline. Return execute()."""
        self.mock_cmdline(cmdline)

        temporary_directory, *_ = self.patch_temporary_directory(3)
        self.write_dsc_example_file(temporary_directory / "hello.dsc")
        (temporary_directory / "log.build").write_text("log file")

        self.patch_upload_artifacts()
        self.patch_upload_work_request_debug_logs()

        self.configure_task()

        return self.task.execute()

    def test_execute_returns_true(self):
        """
        The execute method returns True when cmd returns 0.

        Also check that check_directory_for_consistency_errors() didn't
        return errors.

        Assert that EXECUTE_LOG file is created.
        """
        self.patch_sbuild_debusine()
        self.patch_fetch_input(return_value=True)
        self.patch_configure_for_execution(return_value=True)

        check_directory_for_consistency_errors_mocked = (
            self.patch_check_directory_for_consistency_errors()
        )
        check_directory_for_consistency_errors_mocked.return_value = []
        stdout_output = "stdout output of the command"
        stderr_output = "stderr output of the command"
        cmd = ["sh", "-c", f"echo {stdout_output}; echo {stderr_output} >&2"]
        cmd_quoted = (
            "sh -c 'echo stdout output of the command; "
            "echo stderr output of the command >&2'"
        )

        self.assertTrue(self.run_execute(cmd))

        expected = textwrap.dedent(
            f"""\
                cmd: {cmd_quoted}
                output (contains stdout and stderr):
                {stdout_output}
                {stderr_output}

                aborted: False
                returncode: 0

                Files in working directory:
                hello.dsc
                log.build
                {Sbuild.CMD_LOG_SEPARATOR}
                """
        )

        # debug_log_file contains the expected text
        self.assertEqual(
            (
                Path(self.task._debug_log_files_directory.name)
                / self.task.CMD_LOG_FILENAME
            ).read_text(),
            expected,
        )

        check_directory_for_consistency_errors_mocked.assert_called()

    def test_execute_return_false_no_fetch_required_input_files(self):
        """execute() method returns False: cannot fetch required input files."""
        self.patch_fetch_input(return_value=False)
        self.assertFalse(self.run_execute(["true"]))

    def test_execute_return_false_run_cmd_succeeded_return_false(self):
        """execute() returns False: run_cmd_succeeded() returned False."""
        self.patch_fetch_input(return_value=True)
        self.patch_configure_for_execution(return_value=True)
        with mock.patch.object(
            self.task, "run_cmd_succeeded", return_value=False
        ) as mocked:
            self.assertFalse(self.run_execute(["false"]))

        mocked.assert_called_with(1)

    def test_execute_check_build_consistency_error_returns_errors(self):
        """
        execute() method returns False: check_build_consistency returned errors.

        Assert check_build_consistency() was called.
        """
        self.patch_sbuild_debusine()
        self.patch_fetch_input(return_value=True)
        self.patch_configure_for_execution(return_value=True)

        check_directory_for_consistency_errors_mocked = (
            self.patch_check_directory_for_consistency_errors()
        )
        check_directory_for_consistency_errors_mocked.return_value = [
            "some error"
        ]

        self.assertFalse(self.run_execute(["true"]))

        check_directory_for_consistency_errors_mocked.assert_called()

    def test_execute_returns_false(self):
        """
        The execute method returns False when cmd returns 1.

        Assert check_build_consistency() was not called.
        """
        check_build_consistency_mocked = (
            self.patch_check_directory_for_consistency_errors()
        )

        self.patch_fetch_input(return_value=True)

        self.assertFalse(self.run_execute(["false"]))

        check_build_consistency_mocked.assert_not_called()

    def patch_sbuild_debusine(self) -> MagicMock:
        """Patch self.task.debusine. Return mock."""
        patcher = mock.patch.object(self.task, "debusine", autospec=Debusine)
        mocked = patcher.start()
        self.addCleanup(patcher.stop)

        return mocked

    def patch_fetch_input(self, *, return_value: bool) -> MagicMock:
        """
        Patch self.task.debusine.fetch_input().

        :param return_value: set mocked.return_value = return_value
        :return: the mock
        """
        patcher = mock.patch.object(self.task, "fetch_input", autospec=True)
        mocked = patcher.start()
        mocked.return_value = return_value
        self.addCleanup(patcher.stop)

        return mocked

    def patch_configure_for_execution(self, *, return_value: bool) -> MagicMock:
        """
        Patch self.task.debusine.configure_for_execution().

        :param return_value: set mocked.return_value = return_value
        :return: the mock
        """
        patcher = mock.patch.object(
            self.task, "configure_for_execution", autospec=True
        )
        mocked = patcher.start()
        mocked.return_value = return_value
        self.addCleanup(patcher.stop)

        return mocked

    def patch_upload_artifacts(self):
        """Patch self.task.upload_artifacts."""
        patcher = mock.patch.object(
            self.task, "upload_artifacts", autospec=True
        )
        mocked = patcher.start()
        self.addCleanup(patcher.stop)

        return mocked

    def patch_upload_work_request_debug_logs(self):
        """Patch self.task._upload_work_request_debug_logs."""
        patcher = mock.patch.object(
            self.task, "_upload_work_request_debug_logs", autospec=True
        )
        mocked = patcher.start()
        self.addCleanup(patcher.stop)

        return mocked

    def patch_check_directory_for_consistency_errors(self):
        """Patch self.task.check_directory_for_consistency_errors."""
        patcher = mock.patch.object(
            self.task, "check_directory_for_consistency_errors", autospec=True
        )
        mocker = patcher.start()
        self.addCleanup(patcher.stop)

        return mocker

    def test_execute_call_upload_artifacts(self):
        """
        execute() call methods to upload artifacts.

        It calls: upload_artifacts() and _upload_work_request_debug_logs().
        """
        self.mock_cmdline(["true"])

        self.patch_sbuild_debusine()
        self.patch_fetch_input(return_value=True)
        self.patch_configure_for_execution(return_value=True)

        self.configure_task()

        execute_directory, download_directory = self.patch_temporary_directory(
            2
        )

        upload_artifacts_mocked = self.patch_upload_artifacts()
        self.patch_check_directory_for_consistency_errors().return_value = []

        upload_work_request_debug_logs_patcher = mock.patch.object(
            self.task, "_upload_work_request_debug_logs", autospec=True
        )
        upload_work_request_debug_logs_mocked = (
            upload_work_request_debug_logs_patcher.start()
        )
        self.addCleanup(upload_work_request_debug_logs_patcher.stop)

        self.task.configure_for_execution(download_directory)
        self.task.execute()

        self.assertEqual(upload_artifacts_mocked.call_count, 1)
        upload_artifacts_mocked.assert_called_with(
            execute_directory, execution_success=True
        )

        self.assertEqual(upload_work_request_debug_logs_mocked.call_count, 1)
        upload_work_request_debug_logs_mocked.assert_called_with()

    def test_execute_run_cmd_with_capture_stdout(self):
        """execute() call run_cmd with capture_stdout."""
        self.task.CAPTURE_OUTPUT_FILENAME = "cmd.out"

        self.patch_fetch_input(return_value=True)
        run_cmd_mock = self.patch_run_cmd()
        run_cmd_mock.return_value = False

        self.patch_configure_for_execution(return_value=True)
        self.patch_upload_artifacts()
        self.patch_upload_work_request_debug_logs()

        self.task.execute()

        # run_cmd was ran with:
        # capture_stdout_filename=self.task.CAPTURE_OUTPUT_FILENAME)
        self.assertEqual(
            run_cmd_mock.call_args.kwargs["capture_stdout_filename"],
            self.task.CAPTURE_OUTPUT_FILENAME,
        )

    def test_execute_does_not_run_cmd(self):
        """execute(), no call run_cmd(): configure_for_execution() failed."""
        self.configure_task()

        self.patch_sbuild_debusine()
        self.patch_fetch_input(return_value=True)

        temporary_directory, *_ = self.patch_temporary_directory(3)

        patcher_configure = mock.patch.object(
            self.task, "configure_for_execution"
        )
        configure_for_execution_mocked = patcher_configure.start()
        configure_for_execution_mocked.return_value = False
        self.addCleanup(patcher_configure.stop)

        run_cmd = self.patch_run_cmd()

        self.assertFalse(self.task.execute())

        run_cmd.assert_not_called()

    def test_analyze_worker(self):
        """Test the analyze_worker() method."""
        self.schroot_list_mock.return_value = "chroot:jessie-arm64-sbuild"
        self.dpkg_architecture_mock.return_value = "mipsel"

        metadata = self.task.analyze_worker()

        self.assertEqual(metadata["sbuild:chroots"], ["jessie-arm64"])
        self.assertEqual(metadata["sbuild:host_architecture"], "mipsel")

    def worker_metadata(self):
        """Return worker_metadata with sbuild:version=self.task.TASK_VERSION."""
        return {
            "sbuild:version": self.task.TASK_VERSION,
            "sbuild:chroots": ["bullseye-amd64"],
            "sbuild:host_architecture": "amd64",
        }

    def test_can_run_on_good_case(self):
        """Ensure can_run_on returns True if all conditions are met."""
        worker_metadata = self.worker_metadata()
        self.configure_task()

        self.assertTrue(self.task.can_run_on(worker_metadata))

    def test_can_run_mismatched_task_version(self):
        """Ensure can_run_on returns False for mismatched versions."""
        worker_metadata = self.worker_metadata()
        worker_metadata["sbuild:version"] += 1
        self.configure_task()

        self.assertFalse(self.task.can_run_on(worker_metadata))

    def test_can_run_chroot_not_available(self):
        """Ensure can_run_on returns False when needed chroot is not there."""
        worker_metadata = self.worker_metadata()
        worker_metadata["sbuild:chroots"] = ["jessie-arm64"]
        self.configure_task()

        self.assertFalse(self.task.can_run_on(worker_metadata))

    def test_can_run_missing_distribution(self):
        """can_run_on returns False if sbuild:chroots is not in the metadata."""
        self.configure_task()

        self.assertFalse(self.task.can_run_on({"sbuild:version": 1}))

    def test_upload_artifacts_no_deb_files(self):
        """upload_artifacts() does not try to upload deb files."""
        temp_directory = self.create_temporary_directory()

        debusine_mock = self.patch_sbuild_debusine()

        # Add log.build file: the PackageBuildLog is created
        self.task._dsc_file = self.create_temporary_file(suffix=".dsc")
        dsc = self.write_dsc_example_file(self.task._dsc_file)
        (log_file := temp_directory / "log.build").write_text("log file")

        # Add .changes file: BinaryUpload is created
        changes_file = temp_directory / "python-network.changes"

        (file1 := temp_directory / "package.deb").write_text("test")

        self.write_changes_file(changes_file, [file1])

        self.task.upload_artifacts(temp_directory, execution_success=True)

        self.assertEqual(debusine_mock.upload_artifact.call_count, 2)

        debusine_mock.upload_artifact.assert_has_calls(
            [
                call(
                    PackageBuildLog.create(
                        file=log_file,
                        source=dsc["Source"],
                        version=dsc["Version"],
                    ),
                    workspace=self.task.workspace,
                    work_request=None,
                ),
                call(
                    BinaryUpload.create(
                        changes_file=changes_file, exclude_files=set()
                    ),
                    workspace=self.task.workspace,
                    work_request=None,
                ),
            ]
        )

    def test_upload_artifacts_build_failure(self):
        """upload_artifacts() uploads only the .build file: build failed."""
        temp_directory = self.create_temporary_directory()

        debusine_mock = self.patch_sbuild_debusine()

        # Add log.build file
        self.task._dsc_file = self.create_temporary_file(suffix=".dsc")
        dsc = self.write_dsc_example_file(self.task._dsc_file)
        (log_file := temp_directory / "log.build").write_text("log file")

        self.task.work_request = 5

        self.task.upload_artifacts(temp_directory, execution_success=False)

        # Only one artifact is uploaded: PackageBuildLog
        # (because execution_success=False)
        self.assertEqual(debusine_mock.upload_artifact.call_count, 1)

        debusine_mock.upload_artifact.assert_has_calls(
            [
                call(
                    PackageBuildLog.create(
                        file=log_file,
                        source=dsc["Source"],
                        version=dsc["Version"],
                    ),
                    workspace=self.task.workspace,
                    work_request=self.task.work_request,
                )
            ]
        )

    def test_upload_validation_errors(self):
        """upload_artifacts() does not try to upload deb files."""
        build_directory = self.create_temporary_directory()

        debusine_mock = self.patch_sbuild_debusine()

        # Add log.build file: the PackageBuildLog is created
        self.task._dsc_file = self.create_temporary_file(suffix=".dsc")
        dsc_data = self.write_dsc_example_file(self.task._dsc_file)

        (build_file := build_directory / "file.build").write_text("the build")

        # Add .changes file: BinaryUpload is created
        changes_file = build_directory / "python-network.changes"

        # Add *.deb file
        (file1 := build_directory / "package.deb").write_text("test")
        self.write_changes_file(changes_file, [file1])

        file1.write_text("Hash will be unexpected")

        validation_failed_log_remote_id = 25
        debusine_mock.upload_artifact.return_value = RemoteArtifact(
            id=validation_failed_log_remote_id, workspace="Test"
        )

        # Upload the artifacts
        self.task.upload_artifacts(build_directory, execution_success=True)

        source = dsc_data["Source"]
        version = dsc_data["Version"]

        calls = [
            call(
                PackageBuildLog.create(
                    file=build_file, source=source, version=version
                ),
                workspace=self.task.workspace,
                work_request=None,
            ),
            call(
                BinaryUpload.create(
                    changes_file=changes_file, exclude_files=set()
                ),
                workspace=self.task.workspace,
                work_request=None,
            ),
        ]

        debusine_mock.upload_artifact.assert_has_calls(calls)
        self.assertEqual(debusine_mock.upload_artifact.call_count, len(calls))

    def test_configure_for_execution_no_dsc(self):
        """configure_for_execution() no .dsc: return False."""
        directory = self.create_temporary_directory()

        dsc_file = directory / "hello.dsc"

        self.write_dsc_example_file(dsc_file)

        self.assertTrue(self.task.configure_for_execution(directory))

    def test_configure_for_execution_from_artifact_id_error_too_many_dscs(self):
        """configure_for_execution() raise TaskConfigForBuildError."""
        download_directory = self.create_temporary_directory()

        (dsc_file1 := download_directory / "file1.dsc").write_text("")
        (dsc_file2 := download_directory / "file2.dsc").write_text("")

        dsc_files = sorted([str(dsc_file1), str(dsc_file2)])

        self.task.data["input"] = {"artifact_id": 17}

        self.assertFalse(self.task.configure_for_execution(download_directory))

        log_file_contents = (
            Path(self.task._debug_log_files_directory.name)
            / "configure_for_execution.log"
        ).read_text()

        self.assertEqual(
            log_file_contents,
            f"There must be one *.dsc file. Current files: {dsc_files}",
        )

    def patch_run_cmd(self) -> MagicMock:
        """Patch self.task.run_cmd. Return its mock."""
        patcher_build = mock.patch.object(self.task, "run_cmd")
        mocked = patcher_build.start()
        self.addCleanup(patcher_build.stop)

        return mocked

    def patch_upload_package_build_log(self) -> MagicMock:
        """Patch self.task._upload_package_build_log. Return its mock."""
        patcher = mock.patch.object(
            self.task, "_upload_package_build_log", autospec=True
        )
        mocked = patcher.start()
        self.addCleanup(patcher.stop)
        return mocked

    def test_upload_artifacts_create_relation_build_log_to_source(self):
        """upload_artifacts() create relation from build log to source."""
        temp_directory = self.create_temporary_directory()

        # Add log.build file
        self.task._dsc_file = self.create_temporary_file(suffix=".dsc")
        self.write_dsc_example_file(self.task._dsc_file)

        source_artifact_id = 8
        self.task._source_artifacts_ids = [source_artifact_id]

        upload_package_build_log_mocked = self.patch_upload_package_build_log()
        upload_package_build_log_mocked.return_value = RemoteArtifact(
            id=7, workspace="not-relevant"
        )

        (temp_directory / "log.build").write_text("the log file")

        debusine_mock = self.patch_sbuild_debusine()

        self.task.upload_artifacts(temp_directory, execution_success=False)

        debusine_mock.relation_create.assert_called_with(
            upload_package_build_log_mocked.return_value.id,
            source_artifact_id,
            "relates-to",
        )

    def test_upload_artifacts_search_for_dsc_file(self):
        """
        upload_artifacts() is called with _dsc_file already set.

        The .dsc file was set by configure_for_execution().
        """
        build_directory = self.create_temporary_directory()
        dsc_file = self.create_temporary_file(suffix=".dsc")

        self.patch_sbuild_debusine()

        self.task._dsc_file = dsc_file

        dsc_data = self.write_dsc_example_file(dsc_file)

        upload_package_build_log_mocked = self.patch_upload_package_build_log()

        self.task.upload_artifacts(build_directory, execution_success=False)

        upload_package_build_log_mocked.assert_called_with(
            build_directory, dsc_data["Source"], dsc_data["Version"]
        )

    def test_upload_artifacts_no_dsc_file(self):
        """upload_artifacts() called without an existing .dsc file."""
        build_directory = self.create_temporary_directory()

        debusine_mock = self.patch_sbuild_debusine()

        self.task.upload_artifacts(build_directory, execution_success=False)

        debusine_mock.upload_artifact.assert_not_called()

    def test_upload_artifacts(self):
        """upload_artifacts() call upload_artifact() with the artifacts."""
        temp_directory = self.create_temporary_directory()

        # Add log.build file: the PackageBuildLog is uploaded
        self.task._dsc_file = self.create_temporary_file(suffix=".dsc")

        dsc_data = self.write_dsc_example_file(self.task._dsc_file)

        (build_log_file := temp_directory / "log.build").write_text(
            "the log file"
        )

        debusine_mock = self.patch_sbuild_debusine()

        # Add two files: BinaryPackages is uploaded with them
        (file1 := temp_directory / "hello_amd64.deb").write_text("deb")
        (file2 := temp_directory / "hello_amd64.udeb").write_text("udeb")

        # Add .changes file: BinaryUpload is uploaded
        changes_file = temp_directory / "python-network.changes"
        self.write_changes_file(changes_file, [file1, file2])

        self.task.work_request = 8

        self.task.upload_artifacts(temp_directory, execution_success=True)

        workspace = self.task.workspace

        expected_upload_artifact_calls = [
            call(
                PackageBuildLog.create(
                    file=build_log_file,
                    source=dsc_data["Source"],
                    version=dsc_data["Version"],
                ),
                workspace=workspace,
                work_request=self.task.work_request,
            ),
            call(
                Sbuild._create_binary_package_local_artifact(
                    self.task,
                    temp_directory,
                    dsc=utils.read_dsc(self.task._dsc_file),
                    suffixes=[".deb", ".udeb"],
                ),
                workspace=workspace,
                work_request=self.task.work_request,
            ),
            call(
                BinaryUpload.create(
                    changes_file=changes_file,
                    exclude_files=self.task._packaged_files,
                ),
                workspace=workspace,
                work_request=self.task.work_request,
            ),
        ]
        self.assertEqual(
            debusine_mock.upload_artifact.call_count,
            len(expected_upload_artifact_calls),
        )
        debusine_mock.upload_artifact.assert_has_calls(
            expected_upload_artifact_calls
        )

        # Ensure that _upload_artifacts() tried to create relations
        # (the exact creation of relations is tested in
        # test_create_relations).
        self.assertGreaterEqual(debusine_mock.relation_create.call_count, 1)

    def create_remote_binary_packages_relations(
        self,
    ) -> dict[str, RemoteArtifact]:
        """
        Create RemoteArtifacts and call Sbuild method to create relations.

        Call self.task._create_remote_binary_packages_relations().

        :return: dictionary with the remote artifacts.
        """  # noqa: D402
        workspace = "debian"
        artifacts = {
            "build_log": RemoteArtifact(id=1, workspace=workspace),
            "binary_upload": RemoteArtifact(id=2, workspace=workspace),
            "binary_package": RemoteArtifact(id=3, workspace=workspace),
        }
        self.task._create_remote_binary_packages_relations(
            artifacts["build_log"],
            artifacts["binary_upload"],
            [artifacts["binary_package"]],
        )

        return artifacts

    def assert_create_remote_binary_packages_relations(
        self,
        debusine_mock,
        artifacts: dict[str, RemoteArtifact],
        *,
        source_artifacts_ids: Optional[list[int]] = None,
    ):
        """Assert that debusine_mock.relation_create was called correctly."""
        expected_relation_create_calls = [
            call(
                artifacts["build_log"].id,
                artifacts["binary_package"].id,
                "relates-to",
            ),
            call(
                artifacts["binary_upload"].id,
                artifacts["binary_package"].id,
                "extends",
            ),
            call(
                artifacts["binary_upload"].id,
                artifacts["binary_package"].id,
                "relates-to",
            ),
        ]

        for source_artifact_id in source_artifacts_ids:
            expected_relation_create_calls.append(
                call(
                    artifacts["binary_package"].id,
                    source_artifact_id,
                    "built-using",
                )
            )

        self.assertEqual(
            debusine_mock.relation_create.call_count,
            len(expected_relation_create_calls),
        )

        debusine_mock.relation_create.assert_has_calls(
            expected_relation_create_calls, any_order=True
        )

    def test_create_relations(self):
        """create_relations() call relation_create()."""
        for source_artifacts_ids in [[], [9, 10]]:
            with self.subTest(source_artifacts_ids=source_artifacts_ids):
                debusine_mock = self.patch_sbuild_debusine()

                self.task._source_artifacts_ids = source_artifacts_ids

                artifacts = self.create_remote_binary_packages_relations()

                self.assert_create_remote_binary_packages_relations(
                    debusine_mock,
                    artifacts,
                    source_artifacts_ids=source_artifacts_ids,
                )

    @staticmethod
    def configuration(host_architecture: str, build_components: list[str]):
        """Return configuration with host_architecture and build_components."""
        return {
            "input": {
                "source_artifact_id": 5,
            },
            "distribution": "bullseye",
            "host_architecture": host_architecture,
            "build_components": build_components,
            "sbuild_options": [
                "--post-build-commands="
                "/usr/local/bin/post-process %SBUILD_CHANGES"
            ],
        }

    @staticmethod
    def create_deb_udeb_files(directory: Path, *, architecture: str) -> None:
        """Create deb and udeb files for architecture."""
        (directory / f"hello_{architecture}.deb").write_text("deb")
        (directory / f"hello_{architecture}.udeb").write_text("udeb")

    def create_and_upload_binary_packages(
        self,
        host_architecture: str,
        build_components: list[str],
        *,
        create_debs_for_architectures: list[str],
    ) -> (MagicMock, Path):
        """
        Set environment and call Sbuild._upload_binary_packages().

        Return debusine.upload_artifact mock and Path with the files.
        """
        temp_directory = self.create_temporary_directory()

        dsc_file = temp_directory / "file.dsc"

        self.write_dsc_example_file(dsc_file)

        for architecture in create_debs_for_architectures:
            self.create_deb_udeb_files(
                temp_directory, architecture=architecture
            )

        self.task.configure(
            self.configuration(host_architecture, build_components)
        )

        dsc = utils.read_dsc(dsc_file)

        debusine_mock = self.patch_sbuild_debusine()

        self.task._upload_binary_packages(temp_directory, dsc)

        return debusine_mock.upload_artifact, temp_directory

    def assert_upload_binary_packages_with_files(
        self, upload_artifact_mock: MagicMock, files: list[set[Path]]
    ):
        """
        Assert debusine.upload_artifact() was called with expected artifacts.

        Assert that the number of calls in upload_artifact_mock is the same
        as len(files); and assert that the file names on each upload_artifact()
        are the expected ones (files[i]).
        """
        self.assertEqual(upload_artifact_mock.call_count, len(files))

        for call_args, files in zip(upload_artifact_mock.call_args_list, files):
            expected_filenames = set(file.name for file in files)
            self.assertEqual(call_args[0][0].files.keys(), expected_filenames)

    def test_upload_binary_packages_build_components_any(self):
        r"""
        upload_binary_packages() upload \*_host.deb packages.

        build_components()
        """
        (
            upload_artifact_mock,
            temp_directory,
        ) = self.create_and_upload_binary_packages(
            "amd64", ["any"], create_debs_for_architectures=["amd64", "all"]
        )

        expected_files = temp_directory.glob("*_amd64.*deb")

        self.assert_upload_binary_packages_with_files(
            upload_artifact_mock, [expected_files]
        )

    def test_upload_binary_packages_build_components_all(self):
        r"""
        upload_binary_packages() upload \*_all.deb packages.

        build_components is "all". All packages are created (host and all)
        but only \*_all.deb are uploaded.
        """
        (
            upload_artifact_mock,
            temp_directory,
        ) = self.create_and_upload_binary_packages(
            "amd64", ["all"], create_debs_for_architectures=["amd64", "all"]
        )

        expected_files = temp_directory.glob("*_all.*deb")

        self.assert_upload_binary_packages_with_files(
            upload_artifact_mock, [expected_files]
        )

    def test_upload_binary_packages_build_components_any_all(self):
        r"""upload_binary_packages() upload \*_host.deb and \*_all.deb files."""
        (
            upload_artifact_mock,
            temp_directory,
        ) = self.create_and_upload_binary_packages(
            "amd64",
            ["any", "all"],
            create_debs_for_architectures=["amd64", "all"],
        )

        expected_files_amd64 = temp_directory.glob("*_amd64.*deb")
        expected_files_all = temp_directory.glob("*_all.*deb")

        self.assert_upload_binary_packages_with_files(
            upload_artifact_mock, [expected_files_amd64, expected_files_all]
        )

    def test_upload_binary_packages_build_components_any_all_without_all(self):
        r"""
        upload_binary_packages() upload \*_host.deb packages.

        Only host architecture packages exist: only one package created.
        """
        (
            upload_artifact_mock,
            temp_directory,
        ) = self.create_and_upload_binary_packages(
            "amd64", ["any", "all"], create_debs_for_architectures=["amd64"]
        )

        expected_files_amd64 = temp_directory.glob("*_amd64.*deb")

        self.assert_upload_binary_packages_with_files(
            upload_artifact_mock, [expected_files_amd64]
        )

    def test_create_binary_package_local_artifact(self):
        """
        _create_binary_package_local_artifact(): return BinaryPackage.

        Assert that return the expected BinaryPackages, and add the
        files in self.task.packaged_files.
        """
        build_directory = self.create_temporary_directory()

        dsc_file = build_directory / "package.dsc"

        dsc_data = self.write_dsc_example_file(dsc_file)

        (package_file := build_directory / "package.deb").write_text("")

        binary_package = self.task._create_binary_package_local_artifact(
            build_directory, utils.read_dsc(dsc_file), suffixes=[".deb"]
        )

        self.assertEqual(
            binary_package,
            BinaryPackages.create(
                srcpkg_name=dsc_data["Source"],
                srcpkg_version=dsc_data["Version"],
                version=dsc_data["Version"],
                architecture=dsc_data["Architecture"],
                packages=[],
                files=[package_file],
            ),
        )

        self.assertEqual(self.task._packaged_files, {package_file})
