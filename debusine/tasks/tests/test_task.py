# Copyright 2021 The Debusine developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Unit tests for the base Task class."""
from pathlib import Path
from unittest import TestCase, mock
from unittest.mock import MagicMock

from debusine.artifacts import WorkRequestDebugLogs
from debusine.client.debusine import Debusine
from debusine.tasks import Task, TaskConfigError
from debusine.tasks.sbuild import Sbuild
from debusine.test import TestHelpersMixin


class TestTask(Task):
    """Sample class to test Task class."""


class TestTask2(Task):
    """Test Task class with jsonschema validation."""

    TASK_VERSION = 1

    TASK_DATA_SCHEMA = {
        "type": "object",
        "properties": {
            "foo": {
                "type": "string",
            },
            "bar": {
                "type": "number",
            },
        },
        "required": ["foo"],
    }


class TaskTests(TestHelpersMixin, TestCase):
    """Unit tests for Task class."""

    def setUp(self) -> None:
        """Create the shared attributes."""
        self.task = TestTask()
        self.task2 = TestTask2()

    def tearDown(self):
        """Delete directory to avoid ResourceWarning."""
        if self.task._debug_log_files_directory:
            self.task._debug_log_files_directory.cleanup()

    def test_task_initialize_member_variables(self):
        """Member variables expected used in other methods are initialized."""
        self.assertIsNone(self.task.workspace)
        self.assertEqual(self.task._source_artifacts_ids, [])

    def test_name_is_set(self):
        """task.name is built from class name."""
        self.assertEqual(self.task.name, "testtask")

    def test_logger_is_configured(self):
        """task.logger is available."""
        self.assertIsNotNone(self.task.logger)

    def test_prefix_with_task_name(self):
        """task.prefix_with_task_name does what it says."""
        self.assertEqual(
            self.task.prefix_with_task_name("foobar"),
            f"{self.task.name}:foobar",
        )

    def test_configure_sets_data_attribute(self):
        """task.data is set with configure."""
        task_data = {"foo": "bar"}
        task_data_notifications = {
            "notifications": {"on_failure": [{"channel": "deblts-email"}]}
        }

        self.task.configure({**task_data, **task_data_notifications})

        self.assertEqual(self.task.data, task_data)

    def test_configure_task_data_notification_drop(self):
        """Raise TaskConfigError: "notifications" is not valid."""
        task_data = {"foo": "bar"}
        task_data_notification = {"notifications": 10}

        with self.assertRaises(TaskConfigError):
            self.task.configure({**task_data, **task_data_notification})

    def test_configure_fails_with_bad_schema(self):
        """configure() raises TaskConfigError if schema is not respected."""
        task_data = {}
        with self.assertRaises(TaskConfigError):
            self.task2.configure(task_data)
        # Ensure the data attribute is only set after a successful validation
        self.assertIsNone(self.task2.data)

    def test_configure_works_with_good_schema(self):
        """configure() doesn't raise TaskConfigError."""
        task_data = {"foo": "bar", "bar": 3.14}

        self.task2.configure(task_data)

        self.assertEqual(self.task2.data["foo"], "bar")
        self.assertEqual(self.task2.data["bar"], 3.14)

    def test_analyze_worker_without_task_version(self):
        """analyze_worker() reports an unknown task version."""
        metadata = self.task.analyze_worker()
        self.assertIsNone(metadata["testtask:version"])

    def test_analyze_worker_with_task_version(self):
        """analyze_worker() reports a task version."""
        metadata = self.task2.analyze_worker()
        self.assertEqual(metadata["testtask2:version"], 1)

    def test_analyze_worker_all_tasks(self):
        """analyze_worker_all_tasks() reports results for each task."""
        patcher = mock.patch.object(
            Task, '_sub_tasks', new={'task': TestTask, 'task2': TestTask2}
        )
        patcher.start()
        self.addCleanup(patcher.stop)

        metadata = self.task.analyze_worker_all_tasks()

        self.assertEqual(
            metadata,
            self.task2.analyze_worker() | self.task.analyze_worker(),
        )

        # Assert that metadata contains data
        self.assertNotEqual(metadata, {})

    def test_execute(self):
        """Ensure execute raises NotImplementedError."""
        with self.assertRaises(NotImplementedError):
            self.task.execute()

    def test_can_run_on_no_version(self):
        """Ensure can_run_on returns True if no version is specified."""
        self.assertIsNone(self.task.TASK_VERSION)
        metadata = self.task.analyze_worker()
        self.assertEqual(self.task.can_run_on(metadata), True)

    def test_can_run_on_with_different_versions(self):
        """Ensure can_run_on returns False if versions differ."""
        self.assertIsNone(self.task.TASK_VERSION)
        metadata = self.task.analyze_worker()
        metadata["testtask:version"] = 1
        self.assertEqual(self.task.can_run_on(metadata), False)

    def test_class_for_name_sbuild(self):
        """class_from_name returns Sbuild (case-insensitive)."""
        self.assertEqual(Task.class_from_name('sBuIlD'), Sbuild)

    def test_class_for_name_no_class(self):
        """class_from_name raises a ValueError exception."""
        with self.assertRaisesRegex(
            ValueError, "'non-existing-class' is not a registered task_name"
        ):
            Task.class_from_name('non-existing-class')

    def test_class_for_name_no_duplicates(self):
        """
        Task.__init_subclass__ raises AssertionError for duplicated names.

        Task.__init_subclass__ uses the class name in lowercase: it can cause
        unexpected duplicated names. Check that are detected.
        """
        with self.assertRaises(AssertionError):

            class Somesubclassname(Task):
                pass

            class SomeSubclassName(Task):
                pass

    def test_execute_logging_exceptions_execute(self):
        """Task.execute_logging_exceptions() executes self.execute."""
        patcher = mock.patch.object(self.task, "execute", autospec=True)
        mocker = patcher.start()
        self.addCleanup(patcher.stop)

        return_values = [True, False]
        for return_value in return_values:
            mocker.return_value = return_value
            self.assertEqual(
                self.task.execute_logging_exceptions(), return_value
            )

        self.assertEqual(mocker.call_count, len(return_values))

    def test_execute_logging_exceptions_handle_exception(self):
        """Task.execute_logging_exceptions() logs the exception."""
        patcher = mock.patch.object(self.task, "execute", autospec=True)
        mocker = patcher.start()
        mocker.side_effect = ValueError()
        self.addCleanup(patcher.stop)

        with self.assertRaises(ValueError), self.assertLogs(self.task.logger):
            self.task.execute_logging_exceptions()

    def test_append_to_log_file(self):
        """Task.append_to_lot_file() appends data to a log file."""
        contents = ["a 1", "b 2"]
        contents_str = "\n".join(contents) + "\n"

        filename = "log.txt"

        # Add data into the file
        self.task.append_to_log_file(filename, contents)

        file = Path(self.task._debug_log_files_directory.name) / filename

        self.assertEqual(file.read_text(), contents_str)

        # Add more data
        self.task.append_to_log_file(filename, contents)
        self.assertEqual(file.read_text(), contents_str * 2)

    def test_open_debug_log_file_default_is_append_mode(self):
        """
        Task.open_debug_log_file() return a file in a temporary directory.

        self.task._debug_log_files_directory is set to the temporary directory.

        Calling it again: return the same file (append mode by default).
        """
        line1 = "First line\n"
        filename = "test.log"
        with self.task.open_debug_log_file(filename) as file:
            file.write(line1)

        # Verify Task._debug_log_files_directory contains the file
        directory = Path(self.task._debug_log_files_directory.name)

        self.assertTrue(directory.exists())
        log_file = Path(self.task._debug_log_files_directory.name) / filename

        self.assertEqual(log_file, directory / filename)
        self.assertEqual(log_file.read_text(), line1)

        # Verify by default is in append mode: we are adding to it
        line2 = "Second line\n"
        with self.task.open_debug_log_file(filename) as file:
            file.write(line2)

        self.assertEqual(log_file.read_text(), f"{line1}{line2}")

        self.task._debug_log_files_directory.cleanup()

    def test_open_debug_log_file_mode_is_used(self):
        """Task.open_debug_log_file() use the specified mode."""
        filename = "test.log"

        with self.task.open_debug_log_file(filename) as file:
            file.write("something")

        line = "new line"
        with self.task.open_debug_log_file(filename, mode="w") as file:
            file.write(line)

        file = Path(self.task._debug_log_files_directory.name) / filename
        self.assertEqual(file.read_text(), line)

    def patch_task__execute(self) -> MagicMock:
        """Return mocked object of self.task._execute()."""
        patcher = mock.patch.object(self.task, "_execute", autospec=True)
        mocked = patcher.start()
        self.addCleanup(patcher.stop)
        return mocked

    def patch_task__upload_work_request_debug_logs(self):
        """Return mocked object of self.task.upload_work_request_debug_logs."""
        patcher = mock.patch.object(
            self.task, "_upload_work_request_debug_logs", autospec=True
        )
        mocked = patcher.start()
        self.addCleanup(patcher.stop)
        return mocked

    def task_execute_and_assert(self, expected_value: bool):
        """
        Call task.execute() and assert return value is expected_value.

        Set up mocks and ensure that mocks are used as expected.
        """
        _execute_mocked = self.patch_task__execute()
        _execute_mocked.return_value = expected_value
        upload_work_request_debug_logs = (
            self.patch_task__upload_work_request_debug_logs()
        )

        self.assertEqual(self.task.execute(), expected_value)

        _execute_mocked.assert_called_with()
        upload_work_request_debug_logs.assert_called_with()

    def test_execute_return_true(self):
        """Task.execute() return True and call method upload logs."""
        self.task_execute_and_assert(True)

    def test_execute_return_false(self):
        """Task.execute() return False and call method upload logs."""
        self.task_execute_and_assert(False)

    def setup_upload_work_request_debug_logs(
        self, source_artifacts_ids: list[int] = None
    ):
        """Setup for upload_work_request_debug_logs tests."""  # noqa: D401
        self.task.debusine = mock.create_autospec(spec=Debusine)

        # Add a file to be uploaded
        with self.task.open_debug_log_file("test.log") as file:
            file.write("log")

        self.task._workspace = "System"
        self.task.work_request = 5

        if source_artifacts_ids is None:
            self.task._source_artifacts_ids = []
        else:
            self.task._source_artifacts_ids = source_artifacts_ids

    def test_upload_work_request_debug_logs_with_relation(self):
        """
        Artifact is created and uploaded. Relation is created.

        The relation is from the debug logs artifact to the source_artifact_id.
        """
        remote_artifact_id = 10
        source_artifact_id = 2

        self.setup_upload_work_request_debug_logs([source_artifact_id])

        self.task.debusine.upload_artifact.return_value = MagicMock(
            id=remote_artifact_id
        )

        work_request_debug_logs_artifact = WorkRequestDebugLogs.create(
            files=Path(self.task._debug_log_files_directory.name).glob("*")
        )
        self.task._upload_work_request_debug_logs()

        self.task.debusine.upload_artifact.assert_called_with(
            work_request_debug_logs_artifact,
            workspace=self.task.workspace,
            work_request=self.task.work_request,
        )

        self.task.debusine.relation_create.assert_called_with(
            remote_artifact_id, source_artifact_id, "relates-to"
        )

    def test_upload_work_request_debug_logs(self):
        """Artifact is created and uploaded."""
        self.setup_upload_work_request_debug_logs()

        work_request_debug_logs_artifact = WorkRequestDebugLogs.create(
            files=Path(self.task._debug_log_files_directory.name).glob("*")
        )

        self.task._upload_work_request_debug_logs()

        self.task.debusine.upload_artifact.assert_called_with(
            work_request_debug_logs_artifact,
            workspace=self.task.workspace,
            work_request=self.task.work_request,
        )

    def test_upload_work_request_no_log_files(self):
        """No log files: no artifact created."""
        self.task.debusine = mock.create_autospec(spec=Debusine)

        self.task._upload_work_request_debug_logs()

        self.task.debusine.upload_artifact.assert_not_called()

    def test_quote_cmd(self):
        """Task._quote_cmd() return the expected value."""
        self.assertEqual(
            Task._quote_cmd(["/bin/ls", ">=category", 2, 'test"test']),
            "/bin/ls '>=category' 2 'test\"test'",
        )
