# Copyright 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""utils module contain utilities used by different components of debusine."""

import hashlib
import re
from pathlib import Path
from typing import Optional, Sequence

from debian import deb822

from requests.structures import CaseInsensitiveDict

CALCULATE_HASH_CHUNK_SIZE = 1 * 1024 * 1024


def calculate_hash(file_path: Path, hash_name: str):
    """Return hash (using algorithm hash_name) of file."""
    hasher = hashlib.new(hash_name)

    with open(file_path, "rb") as f:
        while chunk := f.read(CALCULATE_HASH_CHUNK_SIZE):
            hasher.update(chunk)

    return hasher.digest()


def _error_message_invalid_header(header_name, header_value):
    return f'Invalid {header_name} header: "{header_value}"'


def parse_range_header(headers: CaseInsensitiveDict[str]) -> Optional[dict]:
    """Parse headers["Range"]. Return dictionary with information."""
    header_name = "Range"
    header_value = headers.get(header_name)

    if header_value is None:
        return None

    if m := re.match("bytes=([0-9]+)-([0-9]+)", header_value):
        return {"start": int(m.group(1)), "end": int(m.group(2))}

    raise ValueError(_error_message_invalid_header(header_name, header_value))


def parse_content_range_header(headers: dict[str, str]) -> Optional[dict]:
    """Parse headers["Content-Range"]. Return dictionary with information."""
    header_name = "Content-Range"
    header_value = headers.get("Content-Range")

    if header_value is None:
        return None

    if m := re.match(r"bytes ([0-9]+)-([0-9]+)/([0-9]+|\*)", header_value):
        return {
            "start": int(m.group(1)),
            "end": int(m.group(2)),
            "size": "*" if m.group(3) == "*" else int(m.group(3)),
        }
    elif m := re.match(r"bytes \*/([0-9]+)", header_value):
        return {
            "start": "*",
            "end": None,
            "size": int(m.group(1)),
        }
    elif re.match(r"bytes \*/\*", header_value):
        return {
            "start": "*",
            "end": None,
            "size": "*",
        }
    raise ValueError(_error_message_invalid_header(header_name, header_value))


def read_dsc(dsc_path: Optional[Path]) -> Optional[deb822.Dsc]:
    """
    If dsc_path is not None: read the file and return the contents.

    If the dsc does not have at least "source" and "version" return None.
    """
    if dsc_path is None:
        return None

    with open(dsc_path) as dsc_file:
        dsc = deb822.Dsc(dsc_file)

        if "source" in dsc and "version" in dsc:
            # At least "source" and "version" must exist to be a valid
            # dsc file in the context of Sbuild task.
            return dsc

    return None


def read_changes(build_directory: Path) -> deb822.Changes:
    """Find the file .changes in build_directory, read and return it."""
    changes_path = find_file_suffixes(build_directory, [".changes"])

    with open(changes_path) as changes_file:
        changes = deb822.Changes(changes_file)
        return changes


def find_files_suffixes(
    directory: Path, endswith: Sequence[str], *, include_symlinks: bool = False
) -> list[Path]:
    """
    Return files (sorted) ending with any of the endswith in directory.

    :param directory: directory where to search the files
    :param endswith: suffix to return the files from the directory
    :param include_symlinks: if False (default): does not return symbolic links,
      if True return symbolic links

    Find only regular files (no symbolic links, directories, etc.).
    """
    found_files: list[Path] = []

    for file in directory.iterdir():
        if not include_symlinks:
            if file.is_symlink():
                continue

        if not file.is_file():
            continue

        if file.name.endswith(tuple(endswith)):
            found_files.append(file)

    return sorted(found_files)


def find_file_suffixes(
    directory: Path, endswith: Sequence[str]
) -> Optional[Path]:
    """
    Find and return file ending with any of the endswith in directory.

    Finds regular files (no symbolic links, directories, etc.).

    Raise RuntimeError if more than one file could be returned.
    """
    found_files = find_files_suffixes(directory, endswith)

    number_of_files = len(found_files)

    if number_of_files == 1:
        return found_files[0]
    elif number_of_files == 0:
        return None
    else:
        found_files.sort()
        found_files = list(map(str, found_files))
        raise RuntimeError(f"More than one {endswith} file: {found_files}")
