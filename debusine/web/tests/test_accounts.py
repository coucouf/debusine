# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for account (log in, log out)."""

from django.test import TestCase
from django.urls import reverse

from rest_framework import status


class LoginTests(TestCase):
    """Test login functionality and template."""

    def test_view(self):
        """Generic test of the view."""
        response = self.client.get(reverse("accounts:login"))

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Correct title
        self.assertContains(response, "Log in")

        # Labels for the form
        self.assertContains(response, "Username:")
        self.assertContains(response, "Password:")

    def test_login_form_errors(self):
        """If the form has errors: template renders them."""
        response = self.client.post(
            reverse("accounts:login"), {"username": "", "password": ""}
        )

        self.assertContains(
            response,
            "Your username and password didn't match. Please try again.",
        )
        self.assertTrue(response.context["form"].errors)
