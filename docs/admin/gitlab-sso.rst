====================================
Enabling logins with GitLab accounts
====================================


Introduction
------------

Debusine supports OpenID Connect authentication: these are the instructions for
setting up authentication against a GitLab server.


Set up
------

Configure the provider in your Debusine instance
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In your local settings:

.. code-block:: python3

    from debusine.project.utils import read_secret_key
    from debusine.server.signon import providers

    SIGNON_PROVIDERS = [
        # Example using salsa.debian.org
        providers.GitlabProvider(
            # Provider name to use in the Redirect URI
            name="salsa",
            # User-visible name
            label="Salsa",
            # Optional icon (path under the static directory)
            icon="signon/gitlabian.svg",
            # URL to the GitLab instance
            url="https://salsa.debian.org",
            # OIDC parameters
            client_id="<to be filled with GitLab-provided Application ID>",
            client_secret=read_secret_key("/etc/debusine/gitlab-app-secret"),
            scope=("openid", "profile", "email"),
        ),
    ]

    # Auto-create a local user for remote accounts
    SIGNON_AUTO_CREATE_USER = True

    # Auto-associate remote accounts to the current local user
    SIGNON_AUTO_BIND = True

Create ``/etc/debusine/gitlab-app-secret`` with the application secret provided
by GitLab and make sure the file is only readable by the ``debusine-server`` user::

    echo "<to be filled with Gitlab-provided Secret>" > /etc/debusine/gitlab-app-secret
    chmod 0600 /etc/debusine/gitlab-app-secret
    chown debusine-server:debusine-server /etc/debusine/gitlab-app-secret


Create the application in GitLab
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Go to your profile preferences, under "Applications"
2. Create a new application:

   1. Tick "Confidential" checkbox
   2. Select scopes: "openid", "profile", "email"
   3. Redirect URI: ``https://your.debusine.server/accounts/accounts/oidc_callback/$PROVIDER_NAME``
      (note that https is required, and this cannot be deployed over plain http)
   4. Copy the Application ID and the Secret to your Debusine local settings


Restart debusine
~~~~~~~~~~~~~~~~

When everything is set up, restart ``debusine-server``::

    systemctl restart debusine-server

In the login page you should now see the option to log in using the provider
you configured.


User mapping
------------

When using ``SIGNON_AUTO_CREATE_USER``, if you are not logged in and you log in
with an external OIDC provider, a new user is created for you using your GitLab
verified email.

If a local user exists using the same verified email, the external user is
associated with the existing local one, and no new user is created.

When using ``SIGNON_AUTO_BIND``, if you are logged in with an existing user and
you log in again using an external OIDC provider, the external user is
connected automatically with the current local one.
