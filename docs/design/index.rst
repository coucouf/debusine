Debusine design
===============

This section groups together everything that you might want to know to
understand the design behind debusine: from the original goals to the
detailed design documents. We also include description of internal
objects so that you can get a grasp on how the design has been
implemented.

.. toctree::
   :glob:

   goals
   concepts
   ontology
   faq
   *
