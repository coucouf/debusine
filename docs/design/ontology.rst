
.. _ontology:

========
Ontology
========

-------------
For Artifacts
-------------

The categorization of artifacts does not enforce anything on the
structure of the associated files and key-value data. However, there must
be some consistency and rules to be able to a make a meaningful use of the
system.

This document presents the various categories that we use to manage a
Debian-based distribution. For each category, we explain:

* what associated files you can find
* what key-value data you can expect
* what relationships with other artifacts are likely to exist

Category ``debian:source-package``
==================================

This artifact represents a set of files that can be extracted in some
way to provide a file hierarchy containing source code that can be built
into ``debian:binary-packages`` artifact(s).

* Data:

  * name: the name of the source package
  * version: the version of the source package
  * type: the type of the source package

    * ``dpkg`` for a source package that can be extracted with ``dpkg-source -x`` on the ``.dsc`` file

  * dsc-fields: a parsed version of the fields available in the .dsc file

* Files: for the ``dpkg`` type, a ``.dsc`` file and all the files
  referenced in that file
* Relationships: none

Category ``debian:binary-packages``
===================================

This artifact represents the set of binary packages (``.deb`` files and
similar) produced during the build of a source package for a given
architecture.

If the build of a source-package produces binaries of more than one
architecture, one ``debian:binary-packages`` artifact is created for each
architecture, listing only the binary packages for that architecture.

* Data:

  * srcpkg-name: the name of the source package
  * srcpkg-version: the version of the source package
  * version: the version used for the build (can be different from the
    source version in case of binary-only rebuilds)
  * architecture: the architecture that the packages have been built for.
    Can be any real Debian architecture or ``all``.
  * packages: the list of binary packages that are part of the build
    for this architecture.

* Files: one or more ``.deb`` files
* Relationships:

  * built-using: the corresponding ``debian:source-package``
  * built-using: other ``debian:binary-packages`` (for example in the case of
    signed packages duplicating the content of an unsigned package)
  * built-using: other ``debian:source-package`` (general case of Debian's
    ``Built-Using`` field)

Category ``debian:source-upload``
=================================

This artifact represents the upload of a source package. Currently
uploads are always represented with ``.changes`` file but the structure
of the artifact makes it possible to represent other kind of uploads
in the future (like uploads with signed git tags, or some debusine native
internal upload).

* Data:

  * type: the type of the source upload

    * ``dpkg``: for an upload generated out of a ``.changes`` file created
      by ``dpkg-buildpackage``

  * changes-fields: a parsed version of the fields available in the
    ``.changes`` file

* Files:

  * a ``.changes`` file

* Relationships:

  * extends: one ``debian:source-package``

Category ``debian:binary-upload``
=================================

This artifact represents the upload of a set of binary packages. Currently
uploads are always represented with ``.changes`` file but the structure
of the artifact makes it possible to represent other kind of uploads
in the future.

* Data:

  * type: the type of the binary upload

    * ``dpkg``: for an upload generated out of a ``.changes`` file created
      by ``dpkg-buildpackage``

  * changes-fields: a parsed version of the fields available in the
    ``.changes`` file

* Files:

  * a ``.changes`` file

* Relationships:

  * extends: one (or more) ``debian:binary-packages``
  * relates-to: one (or more) ``debian:binary-packages``

Category ``debian:package-build-log``
=====================================

This artifact contains a package's build log and some associated
information about the corresponding package build. It is kept around
for traceability and for diagnostic purposes.

* Data:

  * source: name of the source package built
  * version: version of the source package built
  * filename: name of the log file
  * maybe other information extracted out of the build log (build time,
    disk space used, etc.)

* Files:

  * a single file ``.build`` file

* Relationships:

  * relates-to: one (or more) ``debian:binary-packages`` built
  * relates-to: the corresponding ``debian:source-package`` (if built from a
    source package)

Category ``debusine:work-request-debug-logs``
=============================================

* Files:

  * any number of files containing logs and information to help a debusine user
    understand the WorkRequest output: commands executed, output of the
    commands, etc.

* Relationships:

  * relates-to: the corresponding ``debian:source-package`` (if built from a
    source package)

---------
For Tasks
---------

While tasks are unique in theory, we can have different tasks sharing
some commonalities. In the Debian context in particular, we have different
ways to build Debian packages with different helper programs (sbuild,
pbuilder, etc.) and we want those tasks to reuse the same set of
parameters so that they can be called interchangeably.

This public interface is materialized by a generic task that can be
scheduled by the users and that will run one of the available
implementations that can run on one of the available workers.

This section documents those generic tasks and their interface.

.. _package-build-task:

Task ``PackageBuild``
=====================

A generic task to represent a package build, i.e. the act of transforming
a source package (.dsc) into binary packages (.deb).

The ``task_data`` associated to this task can contain the following keys:

* ``input`` (required): a dictionary describing the input data

  * ``source_artifact_id`` (required): source_artifact_id pointing to a source
    package, it is used to retrieve the source package to build.

* ``distribution`` (required): name of the target distribution
* ``extra_repositories`` (optional): a list of extra repositories to enable.
  Each repository is described by a dictionary with the following
  possible keys:

  * ``sources_list``: a single-line for an APT's sources.list file
  * ``authentication_key`` (optional): the ascii-armored public key used to
    authenticate the repository

* ``host_architecture`` (required): the architecture that we want to build
  for, it defines the architecture of the resulting architecture-specific
  .deb (if any)
* ``build_architecture`` (optional, defaults to the host architecture):
  the architecture on which we want to build the package (implies
  cross-compilation if different from the host architecture). Can be
  explicitly set to the undefined value (Python's ``None`` or javascript's
  ``null``) if we want to allow cross-compilation with any build architecture.
* ``build_components`` (optional, defaults to ``any``): list that can contain
  the following 3 words (cf ``dpkg-buildpackage --build=any,all,source``):

  * ``any``: enables build of architecture-specific .deb
  * ``all``: enables build of architecture-independent .deb
  * ``source``: enables build of the source package (.dsc)
* ``build_profiles``: list of build profiles to enable during package build (cf
  ``dpkg-buildpackage --build-profiles``)

* ``build_options``: value of ``DEB_BUILD_OPTIONS`` during build
* ``build_path`` (optional, default unset): forces the build to happen
  through a path named according to the passed value. When this value
  is not set, there's no restriction on the name of the path.
